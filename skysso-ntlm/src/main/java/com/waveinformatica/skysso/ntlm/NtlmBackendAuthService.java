/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.ntlm;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthResultCode;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivano
 */
@BackendAuthService("NTLM")
public class NtlmBackendAuthService implements IBackendAuthService {

	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		return true;
	}

	@Override
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {
		return new LoginResponse(AuthResultCode.ERROR);
	}

	@Override
	public UserNameType getUserNameType() {
		return UserNameType.TEXT;
	}

	@Override
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
		
		List<AuthenticationLink> links = new ArrayList<AuthenticationLink>(1);
		
		AuthenticationLink link = new AuthenticationLink("auth/skysso/ntlm", "btn-windows", "windows", "Windows");
		links.add(link);
		
		return links;
		
	}
	
}
