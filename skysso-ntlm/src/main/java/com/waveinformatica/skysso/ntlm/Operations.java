/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.ntlm;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ntlmv2.filter.NtlmFilter;

@OperationProvider(namespace = "auth/skysso")
public class Operations {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private Configuration config;
	
	@Operation("ntlm")
	@SkipAuthorization
	public BaseResult authenticate(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		PersistedProperties configProps = config.getCustomProperties("skysso-ntlm");
		
		final Properties props = new Properties();
		Enumeration<String> names = (Enumeration<String>) configProps.propertyNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			if (configProps.getProperty(name).trim().length() > 0) {
				props.setProperty(name, configProps.getProperty(name).trim());
			}
		}
		
		NtlmFilter ntlmFilter = new NtlmFilter();
		
		ntlmFilter.init(new FilterConfig() {
			@Override
			public String getFilterName() {
				return "NTLM Filter";
			}
			
			@Override
			public ServletContext getServletContext() {
				return Context.get().getServletContext();
			}
			
			@Override
			public String getInitParameter(String string) {
				return props.getProperty(string);
			}
			
			@Override
			public Enumeration getInitParameterNames() {
				return props.propertyNames();
			}
		});
		
		final Map<String, BaseResult> results = new HashMap<String, BaseResult>();
		
		ntlmFilter.doFilter(request, response, new FilterChain() {
			
			@Override
			public void doFilter(ServletRequest sr, ServletResponse sr1) throws IOException, ServletException {
				MessageResult localResult = factory.newInstance(MessageResult.class);
				localResult.setMessage("Successfully authenticated");
				localResult.setType(MessageResult.MessageType.CONFIRM);
				localResult.setConfirmAction("");
				//((HttpServletResponse) sr1).sendRedirect(((HttpServletRequest) sr).getContextPath() + "/");
				
				results.put("result", localResult);
			}
			
		});
		
		return results.get("result");
		
	}
	
}
