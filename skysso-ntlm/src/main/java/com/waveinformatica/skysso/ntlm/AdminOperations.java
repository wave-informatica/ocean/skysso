/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.ntlm;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.skysso.web.annotations.RequiresPermission;
import com.waveinformatica.skysso.web.dto.Permissions;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "admin/sso")
public class AdminOperations {
	
	@Inject
	private Configuration config;
	
	@Inject
	private ObjectFactory factory;
	
	@Operation("ntlm")
	@RequiresPermission(Permissions.SYSADMIN)
	public TableResult settings() {
		
		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("admin/");
		
		PersistedProperties props = config.getCustomProperties("skysso-ntlm");
		
		if (props.isEmpty()) {
			props.setProperty("ntlm-password", "");
			props.setProperty("ntlm-account", "");
			props.setProperty("ntlm-domain", "");
			props.setProperty("ntlm-dc-name", "");
			props.setProperty("ntlm-dc", "");
			try {
				props.store("");
			} catch (IOException ex) {
				Logger.getLogger(AdminOperations.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		
		result.setTableData(props);
		result.getColumn("value").setEditable(TableResult.CellEditType.TEXT, "admin/sso/ldapEdit");
		
		return result;
		
	}
	
	@Operation("ldapEdit")
	@RequiresPermission(Permissions.SYSADMIN)
	public MessageResult editSettings(@Param("pk") String name, @Param("value") String value) {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		PersistedProperties props = config.getCustomProperties("skysso-ntlm");
		
		props.setProperty(name, value);
		try {
			props.store("");
		} catch (IOException ex) {
			Logger.getLogger(AdminOperations.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		result.setMessage("Configuration successfully stored");
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setConfirmAction("admin/sso/ntlm");
		
		return result;
		
	}
	
}
