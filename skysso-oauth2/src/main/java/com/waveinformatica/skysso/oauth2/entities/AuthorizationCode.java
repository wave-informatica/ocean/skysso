/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.oauth2.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Ivano
 */
@Entity
@Table(name = "skysso_oauth2_authz_codes")
public class AuthorizationCode implements Serializable {
    
    @Embeddable
    public static class Key implements Serializable {
	
	@Column(name = "user_id")
	private Long userId;
	
	@Column(name = "app_name")
	private String appName;
	
	@Column
	private String domain;

	public Long getUserId() {
	    return userId;
	}

	public void setUserId(Long userId) {
	    this.userId = userId;
	}

	public String getAppName() {
	    return appName;
	}

	public void setAppName(String appName) {
	    this.appName = appName;
	}

	public String getDomain() {
	    return domain;
	}

	public void setDomain(String domain) {
	    this.domain = domain;
	}
	
    }
    
    @EmbeddedId
    private Key key = new Key();

    @Column(name = "auth_code")
    private String authCode;

    public Key getKey() {
	return key;
    }

    public void setKey(Key key) {
	this.key = key;
    }

    public Long getUserId() {
	return key.getUserId();
    }

    public void setUserId(Long userId) {
	key.setUserId(userId);
    }

    public String getAppName() {
	return key.getAppName();
    }

    public void setAppName(String appName) {
	key.setAppName(appName);
    }

    public String getDomain() {
	return key.getDomain();
    }

    public void setDomain(String domain) {
	key.setDomain(domain);
    }

    public String getAuthCode() {
	return authCode;
    }

    public void setAuthCode(String authCode) {
	this.authCode = authCode;
    }
    
}
