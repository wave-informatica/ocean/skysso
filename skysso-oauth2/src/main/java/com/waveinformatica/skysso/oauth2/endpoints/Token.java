/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.oauth2.endpoints;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.LoginEvent;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.HttpErrorResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.skysso.oauth2.dao.OAuth2Dao;
import com.waveinformatica.skysso.oauth2.entities.AuthorizationCode;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.Application;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.Domain;
import java.io.IOException;
import java.util.Date;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "oauth2")
public class Token {

    @Inject
    private ObjectFactory factory;
    
    @Inject
    private CoreController controller;
    
    @Inject
    private UserSession userSession;

    @Operation("token")
    @SkipAuthorization
    public BaseResult getToken(HttpServletRequest request, HttpServletResponse response) throws OperationNotFoundException, OAuthSystemException, IOException {

	if (!request.getMethod().equals("POST")) {
	    throw new OperationNotFoundException("Invalid method " + request.getMethod());
	}
	
	SkySSOService service = controller.getService(SkySSOService.class);

	try {
	    
	    OAuthTokenRequest oauthRequest = new OAuthTokenRequest(request);
	    OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
	    
	    Application app = service.getDomainApplication(request, oauthRequest.getClientId());
	    
	    // check if client_secret is valid
	    if (!service.checkApplicationSecret(app, oauthRequest.getClientSecret())) {
		return null;
	    }

	    // do checking for different grant types
	    if (oauthRequest.getParam(OAuth.OAUTH_GRANT_TYPE).equals(GrantType.AUTHORIZATION_CODE.toString())) {
		
		final AuthorizationCode code = factory.newInstance(OAuth2Dao.class).checkAuthorizationCode(oauthRequest.getParam(OAuth.OAUTH_CODE));
		if (code == null) {
		    return null;
		}
                
                Domain domain = service.getDomain(request);
		
                AuthenticatedUser user = new AuthenticatedUser();
                user.setAuthBackend("oauth2");
                user.setId(code.getUserId());
                user.setDomain(domain);
                
                OceanUser oceanUser = service.validateUser(user);
                if (oceanUser != null) {
                    userSession.login(oceanUser);
                }
		
	    } else if (oauthRequest.getParam(OAuth.OAUTH_GRANT_TYPE).equals(GrantType.PASSWORD.toString())) {
		
		LoginEvent evt = new LoginEvent(this, oauthRequest.getUsername(), oauthRequest.getPassword(), request, response);
		controller.dispatchEvent(evt);
		if (evt.isManaged() && evt.getLoggedUser() == null) {
		    return null;
		}
		
		userSession.login(evt.getLoggedUser());
		
	    } else if (oauthRequest.getParam(OAuth.OAUTH_GRANT_TYPE).equals(GrantType.REFRESH_TOKEN.toString())) {
		// refresh token is not supported in this implementation
		throw new UnsupportedOperationException("Refresh token operation is not yet supported");
	    }

	    final String accessToken = oauthIssuerImpl.accessToken();
	    Date expiration = service.storeApplicationToken(app, userSession.getPrincipal().getId(), accessToken, null);

	    OAuthResponse oauthResp = OAuthASResponse
		    .tokenResponse(HttpServletResponse.SC_OK)
		    .setAccessToken(accessToken)
		    .setExpiresIn(((Long) ((expiration.getTime() - new Date().getTime()) / 1000L)).toString())
		    .buildJSONMessage();
	    
	    response.setStatus(oauthResp.getResponseStatus());
	    response.setContentType("application/json");
	    response.getWriter().write(oauthResp.getBody());
	    return null; // Provvisorio

	} catch (OAuthProblemException e) {
	    OAuthResponse res = OAuthASResponse
		    .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
		    .error(e)
		    .buildJSONMessage();
	    
	    response.setStatus(res.getResponseStatus());
	    response.setContentType("application/json");
	    response.getWriter().write(res.getBody());
	}

	return null;

    }

}
