/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.oauth2.endpoints;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.skysso.oauth2.dao.OAuth2Dao;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dao.AuthorizationsDao;
import com.waveinformatica.skysso.web.dto.Application;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.entities.TokenEntity;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.ResponseType;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "oauth2")
public class CrossApplicationAuth {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager sm;
	
	@Operation(value = "authorizeApp", defaultOutputType = MimeTypes.JSON)
	@SkipAuthorization
	public Map<String, String> authenticateApplication(
		    @Param("token") String token,
		    HttpServletRequest request) {
		
		OAuthIssuerImpl oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
		
		Map<String, String> result = new HashMap<String, String>();
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		Domain domain = service.getDomain(request);
		
		AuthorizationsDao authDao = factory.newInstance(AuthorizationsDao.class);
		
		final TokenEntity tokenEntity = authDao.getTokenObject(domain.getName(), token);
		
		if (tokenEntity == null) {
			result.put("error", "invalid_token");
			return result;
		}
		
		sm.authenticateRequest(new OceanUser() {
			private static final long serialVersionUID = 1L;
			@Override
			public Long getId() {
				return tokenEntity.getUserId();
			}
		});
		
		try {
			
			OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(request);
			
			Application app = service.getDomainApplication(request, oauthRequest.getClientId());
			if (app != null) {
				
				Boolean tmpAuth = service.isApplicationAuthorized(app);
				
				if (tmpAuth == null) {
					// Ritornare la richiesta di autorizzazione
					result.put("error", "request_user_authorization");
					return result;
				}
				else if (Boolean.FALSE.equals(tmpAuth)) {
					result.put("error", "user_rejected_authorization");
					return result;
				}
				else {
					
					String responseType = oauthRequest.getParam(OAuth.OAUTH_RESPONSE_TYPE);
					
					if (responseType.equals(ResponseType.CODE.toString())) {
						String authorizationCode = oauthIssuerImpl.authorizationCode();
						factory.newInstance(OAuth2Dao.class).storeAuthorizationCode(app, tokenEntity.getUserId(), authorizationCode);
						result.put("authorization_code", authorizationCode);
						return result;
					}
					else {
						result.put("error", "invalid_request");
						return result;
					}
					
				}
				
			}
			else {
				result.put("error", "target_application_not_found");
				return result;
			}
			
		} catch (OAuthSystemException ex) {
			Logger.getLogger(CrossApplicationAuth.class.getName()).log(Level.SEVERE, null, ex);
			result.put("error_details", ex.getMessage());
			result.put("error", "unexpected_error");
		} catch (OAuthProblemException ex) {
			Logger.getLogger(CrossApplicationAuth.class.getName()).log(Level.SEVERE, null, ex);
			result.put("error_details", ex.getMessage());
			result.put("error", "unexpected_error");
		}
		
		return result;
		
	}
	
}
