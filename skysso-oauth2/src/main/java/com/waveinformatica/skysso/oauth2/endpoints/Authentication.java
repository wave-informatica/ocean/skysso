/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.oauth2.endpoints;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.skysso.oauth2.dao.OAuth2Dao;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.Application;
import java.net.URI;
import java.net.URISyntaxException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.ResponseType;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "oauth2")
public class Authentication {
	
	private static final String AUTHORIZATION_SESSION = "skysso-oauth2-authorization";
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private UserSession userSession;
	
	@Inject
	private OceanSession session;
	
	@Inject
	private CoreController controller;
	
	private SkySSOService service;
	
	@Operation("authorize")
	public BaseResult authorize(@Param("authorize") Long authorize, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException, OAuthSystemException {
		
		service = controller.getService(SkySSOService.class);
		
		OAuthIssuerImpl oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
		
		try {
			//dynamically recognize an OAuth profile based on request characteristic (params,
			// method, content type etc.), perform validation
			OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(request);
			
			boolean authorized = false;
			
			Application app = service.getDomainApplication(request, oauthRequest.getClientId());
			if (app != null) {
				Boolean tmpAuth = service.isApplicationAuthorized(app);
				if (tmpAuth == null && authorize == null) {
					// Ritornare la richiesta di autorizzazione
					MessageResult result = factory.newInstance(MessageResult.class);
					result.setParentOperation(oauthRequest.getParam(OAuth.OAUTH_REDIRECT_URI));
					result.setMessage("L'applicazione \"" + app.getDisplayName() + "\" ha richiesto l'accesso alle tue informazioni sul dominio \"" + app.getDomain().getName() + "\". Desideri autorizzare la richiesta?");
					result.setType(MessageResult.MessageType.WARNING);
					result.setCancelAction(result.getParentOperation());
					result.setConfirmAction(getAuthorizationConfirmUrl(request));
					return result;
				}
				else if (tmpAuth == null && verifyAuthorizationCommand(authorize)) {
					authorized = true;
					service.setApplicationAuthorization(app, authorized);
				}
				else if (tmpAuth != null) {
					authorized = tmpAuth;
				}
			}
			
			String responseType = oauthRequest.getParam(OAuth.OAUTH_RESPONSE_TYPE);
			
			OAuthASResponse.OAuthAuthorizationResponseBuilder builder = OAuthASResponse
				    .authorizationResponse(request, HttpServletResponse.SC_FOUND);
			
			if (authorized && responseType.equals(ResponseType.CODE.toString())) {
				String authorizationCode = oauthIssuerImpl.authorizationCode();
				factory.newInstance(OAuth2Dao.class).storeAuthorizationCode(app, userSession.getPrincipal().getId(), authorizationCode);
				builder.setCode(authorizationCode);
			}
			
			String redirectURI = oauthRequest.getParam(OAuth.OAUTH_REDIRECT_URI);
			
			final OAuthResponse oauthResp = builder.location(redirectURI).buildQueryMessage();
			URI url = new URI(oauthResp.getLocationUri());
			
			response.setStatus(oauthResp.getResponseStatus());
			return new RedirectResult(url.toString());
			
		} catch (OAuthProblemException e) {
			
			String redirectUri = e.getRedirectUri();
			
			if (OAuthUtils.isEmpty(redirectUri)) {
				throw new RuntimeException("OAuth callback url needs to be provided by client!!!");
				//throw new WebApplicationException(
				//	responseBuilder.entity("OAuth callback url needs to be provided by client!!!").build());
			}
			final OAuthResponse oauthResp = OAuthASResponse.errorResponse(HttpServletResponse.SC_FOUND)
				    .error(e)
				    .location(redirectUri).buildQueryMessage();
			final URI location = new URI(oauthResp.getLocationUri());
			
			return new RedirectResult(location.toString());
		}
	}
	
	private String getAuthorizationConfirmUrl(HttpServletRequest request) {
		
		Long token = System.currentTimeMillis();
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("oauth2/authorize");
		if (!request.getQueryString().startsWith("?")) {
			builder.append('?');
		}
		builder.append(request.getQueryString());
		builder.append("&authorize=");
		builder.append(token);
		
		session.put(AUTHORIZATION_SESSION, token);
		
		return builder.toString();
		
	}
	
	private boolean verifyAuthorizationCommand(Long authorization) {
		Long token = session.get(AUTHORIZATION_SESSION, Long.class);
		session.remove(AUTHORIZATION_SESSION);
		if (token == null || !token.equals(authorization)) {
			return false;
		}
		return true;
	}
	
}
