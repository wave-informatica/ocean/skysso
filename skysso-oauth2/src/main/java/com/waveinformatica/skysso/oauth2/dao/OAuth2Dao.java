/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.oauth2.dao;

import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.oauth2.entities.AuthorizationCode;
import com.waveinformatica.skysso.web.dto.Application;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 *
 * @author Ivano
 */
public class OAuth2Dao {
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	public void storeAuthorizationCode(Application app, Long userId, String authCode) {
		
		boolean replace = false;
		AuthorizationCode auth;
		
		try {
			
			auth = em.createQuery("select x from AuthorizationCode x where x.key.userId = :userId and x.key.domain = :domain and x.key.appName = :appName", AuthorizationCode.class)
				    .setParameter("userId", userId)
				    .setParameter("domain", app.getDomain().getName())
				    .setParameter("appName", app.getSysName())
				    .getSingleResult();
			
			replace = true;
			
		} catch (NoResultException e) {
			auth = new AuthorizationCode();
			
			auth.setAppName(app.getSysName());
			auth.setDomain(app.getDomain().getName());
			auth.setUserId(userId);
		}
		
		auth.setAuthCode(authCode);
		
		em.getTransaction().begin();
		
		try {
			
			if (replace) {
				em.merge(auth);
			}
			else {
				em.persist(auth);
			}
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LoggerUtils.getLogger(OAuth2Dao.class).log(Level.SEVERE, "Error saving authorization", e);
		}
	}
	
	public AuthorizationCode checkAuthorizationCode(String code) {
		
		AuthorizationCode auth = null;
		
		try {
			
			auth = em.createQuery("select x from AuthorizationCode x where x.authCode = :code", AuthorizationCode.class)
				    .setParameter("code", code)
				    .getSingleResult();
			
		} catch (Exception e) {
			LoggerUtils.getLogger(OAuth2Dao.class).log(Level.SEVERE, "Error retrieving authorization code", e);
			return null;
		}
		
		if (auth != null) {
			
			em.getTransaction().begin();
			
			try {
				
				em.remove(auth);
				
				em.getTransaction().commit();
				
			} catch (Exception e) {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
				LoggerUtils.getLogger(OAuth2Dao.class).log(Level.SEVERE, "Error cleaning authorization code", e);
				return null;
			}
		}
		
		return auth;
	}
}
