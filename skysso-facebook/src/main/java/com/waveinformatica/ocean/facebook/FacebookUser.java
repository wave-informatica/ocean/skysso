/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.facebook;

import com.restfb.Facebook;
import com.restfb.types.CoverPhoto;
import com.restfb.types.NamedFacebookType;
import com.restfb.types.User;

/**
 *
 * @author Ivano
 */
public class FacebookUser {
	
	@Facebook("id")
	Long id;
	
	@Facebook("birthday")
	String birthday;
	
	@Facebook("email")
	String email;
	
	@Facebook("first_name")
	String firstName;
	
	@Facebook("middle_name")
	String middleName;
	
	@Facebook("last_name")
	String lastName;
	
	@Facebook("name")
	String fullName;
	
	@Facebook("gender")
	String gender;
	
	@Facebook
	CoverPhoto cover;
	
	@Facebook
	String bio;
	
	@Facebook
	String about;
	
	@Facebook("age_range")
	User.AgeRange ageRange;
	
	@Facebook
	String website;
	
	@Facebook
	String locale;
	
	@Facebook
	NamedFacebookType location;
	
	@Facebook("location")
	String locationAsString;
	
	@Facebook
	NamedFacebookType hometown;
	
	@Facebook("hometown")
	String hometownAsString;
	
}
