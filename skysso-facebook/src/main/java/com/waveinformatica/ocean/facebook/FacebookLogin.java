/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.facebook;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.FacebookClient.AccessToken;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.scope.FacebookPermissions;
import com.restfb.scope.ScopeBuilder;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@BackendAuthService("FACEBOOK")
@OperationProvider(namespace = "sso/oauth2")
public class FacebookLogin implements IBackendAuthService {
	
	@Inject
	private Configuration config;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager sm;
	
	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		PersistedProperties props = config.getCustomProperties("skysso.facebook");
		return StringUtils.isNotBlank(props.getProperty("facebook.appid")) &&
			  StringUtils.isNotBlank(props.getProperty("facebook.secret"));
	}

	@Override
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {
		return null;
	}

	@Override
	public UserNameType getUserNameType() {
		return null;
	}

	@Override
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
		
		ScopeBuilder scopeBuilder = new ScopeBuilder();
		scopeBuilder.addPermission(FacebookPermissions.PUBLIC_PROFILE);
		scopeBuilder.addPermission(FacebookPermissions.USER_HOMETOWN);
		scopeBuilder.addPermission(FacebookPermissions.USER_BIRTHDAY);
		scopeBuilder.addPermission(FacebookPermissions.USER_LOCATION);
		scopeBuilder.addPermission(FacebookPermissions.EMAIL);
		
		PersistedProperties props = config.getCustomProperties("skysso.facebook");
		
		HttpServletRequest request = Context.get().getRequest();
		
		String redirect = config.getSiteProperty("site.url", "http://localhost:8080");
		if (redirect.endsWith("/")) {
			redirect = redirect.substring(0, redirect.length() - 1);
		}
		if (!redirect.endsWith(request.getContextPath())) {
			redirect = redirect + request.getContextPath();
		}
		redirect = redirect + "/sso/oauth2/login";
		
		FacebookClient client = new DefaultFacebookClient(Version.LATEST);
		String loginUrl = client.getLoginDialogUrl(
			  props.getProperty("facebook.appid"),
			  redirect,
			  scopeBuilder
		);
		
		List<AuthenticationLink> links = new ArrayList<AuthenticationLink>();
		links.add(new AuthenticationLink(loginUrl, "btn-facebook", "facebook", "Facebook"));
		return links;
	}
	
	@Operation("login")
	@SkipAuthorization
	public BaseResult facebookLoggedIn(@Param("code") String code, HttpServletRequest request) {
		
		PersistedProperties props = config.getCustomProperties("skysso.facebook");
		
		FacebookClient client = new DefaultFacebookClient(Version.VERSION_2_7);
		
		String redirect = config.getSiteProperty("site.url", "http://localhost:8080");
		if (redirect.endsWith("/")) {
			redirect = redirect.substring(0, redirect.length() - 1);
		}
		if (!redirect.endsWith(Context.get().getRequest().getContextPath())) {
			redirect = redirect + Context.get().getRequest().getContextPath();
		}
		redirect = redirect + "/sso/oauth2/login";
		
		AccessToken token = client.obtainUserAccessToken(
			  props.getProperty("facebook.appid"),
			  props.getProperty("facebook.secret"),
			  redirect,
			  code);
		
		client = new DefaultFacebookClient(token.getAccessToken(), Version.VERSION_2_12);
		
		FacebookUser usr = client.fetchObject(
			  "me",
			  FacebookUser.class,
			  Parameter.with("fields", "email,first_name,middle_name,last_name,birthday,gender,cover,about,age_range,website,locale,location,hometown")
		);
		
		AuthenticatedUser user = new AuthenticatedUser(usr.email, usr.fullName);
		user.getFields().put("token", token.getAccessToken());
		user.getFields().put("facebook_id", usr.id);
		user.getFields().put("firstName", usr.firstName);
		user.getFields().put("middleName", usr.middleName);
		user.getFields().put("lastName", usr.lastName);
		user.getFields().put("birthday", usr.birthday);
		user.getFields().put("gender", usr.gender);
		user.getFields().put("cover", usr.cover.getSource());
		user.getFields().put("about", usr.about);
		user.getFields().put("website", usr.website);
		user.getFields().put("locale", usr.locale);
		if (usr.location != null) {
			user.getFields().put("location", usr.location.getName());
		}
		else {
			user.getFields().put("location", usr.locationAsString);
		}
		if (usr.hometown != null) {
			user.getFields().put("hometown", usr.hometown.getName());
		}
		else {
			user.getFields().put("hometown", usr.hometownAsString);
		}
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		user.setDomain(service.getDomain(request));
		user.setAuthBackend("FACEBOOK");
		user.setAuthDate(new Date());
		
		OceanUser oceanUser = service.validateUser(user);
		
		if (!sm.isLogged() && oceanUser != null) {
			sm.getUserSession().login(oceanUser);
		}
		
		return new RedirectResult("~");
		
	}
	
}
