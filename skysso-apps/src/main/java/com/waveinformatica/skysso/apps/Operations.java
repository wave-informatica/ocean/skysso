/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.apps;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.Application;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Ivano
 */
@OperationProvider
public class Operations {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Operation("appsAuth")
	@RootOperation(iconUrl = "briefcase", title = "appsAuthorizations")
	public WebPageResult appsAuthorizations(HttpServletRequest request) {
		
		WebPageResult result = factory.newInstance(WebPageResult.class);
		result.setParentOperation("");
		result.setTemplate("/templates/appsAuthorizations.tpl");
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		List<Application> apps = service.getDomainApplications(request);
		
		List<AppModel> models = new ArrayList<>(apps.size());
		for (Application a : apps) {
			AppModel model = new AppModel();
			model.setApp(a);
			model.setAuthorized(service.isApplicationAuthorized(a));
			models.add(model);
		}
		
		result.setData(models);
		
		result.putExtra("page-title", "Authorizations");
		result.putExtra("page-subtitle", "Manage your applications authorizations");
		
		Results.addCssSource(result, "ris/libs/bootstrap-toggle/bootstrap-toggle.min.css");
		Results.addScriptSource(result, "ris/libs/bootstrap-toggle/bootstrap-toggle.min.js");
		
		return result;
		
	}
	
	@Operation("toggleApp")
	public AppModel toggleApp(
		    @Param("app") String appName,
		    @Param("authorized") Boolean authorized,
		    HttpServletRequest request) {
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		Application  app = service.getDomainApplication(request, appName);
		
		service.setApplicationAuthorization(app, authorized);
		
		AppModel model = new AppModel();
		model.setApp(app);
		model.setAuthorized(authorized);
		
		return model;
		
	}
	
	public static class AppModel extends BaseResult {
		
		private Application app;
		private Boolean authorized;

		public Application getApp() {
			return app;
		}

		public void setApp(Application app) {
			this.app = app;
		}

		public Boolean getAuthorized() {
			return authorized;
		}

		public void setAuthorized(Boolean authorized) {
			this.authorized = authorized;
		}
		
	}
}
