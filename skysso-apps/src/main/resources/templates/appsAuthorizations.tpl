<#if fullResult>
<#include "/templates/includes/header.tpl">
</#if>

<table class="table table-bordered table-striped table-condensed table-hover">
    <#list data as app>
    <tr>
        <td>${app.app.displayName?html}</td>
        <td style="text-align: right;"><input type="checkbox" value="${app.app.sysName?html}" class="app-auth-toggle" data-toggle="toggle" data-on="<@i18n key="authorized" />" data-off="<@i18n key="notAuthorized" />" data-size="small" data-onstyle="success" data-offstyle="warning"<#if app.authorized?? && app.authorized> checked</#if>></td>
    </tr>
    </#list>
</table>

<script type="text/javascript">
    $(document).ready(function () {
        
        $('.app-auth-toggle').change(function () {

            var me = $(this);

            $.get('toggleApp', {
                app: me.attr('value'),
                authorized: me.prop('checked')
            }, function (data) {
                me.prop('checked', data.authorized);
            }, 'json');
            
        });
        
    });
</script>

<#if fullResult>
<#include "/templates/includes/footer.tpl">
</#if>