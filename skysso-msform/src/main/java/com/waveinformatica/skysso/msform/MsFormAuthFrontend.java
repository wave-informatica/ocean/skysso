/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.msform;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.security.OceanAuthenticationHandler;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.UrlBuilder;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.FrontendAuthService;
import com.waveinformatica.skysso.web.base.IFrontendAuthService;
import com.waveinformatica.skysso.web.dto.Domain;

@FrontendAuthService(priority = 1)
public class MsFormAuthFrontend implements IFrontendAuthService {
	
	private static final Object[] managedUAs = {
		"Microsoft Data Access Internet Publishing Provider",
		"Microsoft Office Protocol Discovery",
		"Microsoft-WebDAV-MiniRedir",
		"Non-browser",
		"MSOffice 12",
		Pattern.compile("Mozilla\\/4.0 \\(compatible; MS FrontPage .+")
	};
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private Configuration config;

	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		return true;
	}

	@Override
	public boolean handleRequest(Domain domain, WebRequestEvent requestEvent, SecurityConfigurationEvent evt,
			String opName) {
		
		boolean activate = "t".equalsIgnoreCase(requestEvent.getRequest().getHeader("X-FORMS_BASED_AUTH_ACCEPTED"));
		
		if (!activate) {
			
			final String ua = requestEvent.getRequest().getHeader("User-Agent");
			
			for (Object o : managedUAs) {
				if (o instanceof String && ((String) o).equalsIgnoreCase(ua)) {
					activate = true;
					break;
				}
				else if (o instanceof Pattern) {
					Matcher m = ((Pattern) o).matcher(ua);
					if (m.find()) {
						activate = true;
						break;
					}
				}
			}
			
		}
		
		if (activate) {
			String siteUrl = config.getSiteProperty("site.url", "http://localhost:8080");
			if (siteUrl.endsWith("/")) {
				siteUrl = siteUrl.substring(0, siteUrl.length() - 1);
			}
			String contextPath = requestEvent.getRequest().getContextPath();
			if (StringUtils.isNotBlank(contextPath) && !siteUrl.endsWith(contextPath)) {
				siteUrl += contextPath;
			}
			
			OceanAuthenticationHandler oceanHandler = factory.newInstance(OceanAuthenticationHandler.class);
			RedirectResult result = (RedirectResult) oceanHandler.getAuthenticationResult(requestEvent, evt, opName);
			UrlBuilder builder = new UrlBuilder(result.getRedirect());
			String[] vars = builder.getParameters().toString().split("&");
			String redirect = null;
			for (String v : vars) {
				String[] parts = v.split("=");
				if ("redirect".equals(parts[0])) {
					try {
						redirect = URLDecoder.decode(parts[1], "UTF-8");
					} catch (UnsupportedEncodingException e) {
						LoggerUtils.getLogger(MsFormAuthFrontend.class).log(Level.SEVERE, null, e);
					}
					break;
				}
			}
			if (StringUtils.isBlank(redirect)) {
				redirect = "";
			}
			try {
				new URL(redirect);
			} catch (MalformedURLException e) {
				if (!redirect.startsWith("/")) {
					redirect = "/" + redirect;
				}
				redirect = siteUrl + redirect;
			}
			String authRedirect = result.getRedirect();
			if (authRedirect.startsWith("~")) {
				authRedirect = authRedirect.substring(1);
			}
			try {
				new URL(authRedirect);
			} catch (MalformedURLException e) {
				if (!authRedirect.startsWith("/")) {
					authRedirect = "/" + authRedirect;
				}
				authRedirect = siteUrl + authRedirect;
			}
			requestEvent.getResponse().setStatus(403);
			requestEvent.getResponse().setHeader("X-Forms_Based_Auth_Required", authRedirect);
			requestEvent.getResponse().setHeader("X-Forms_Based_Auth_Return_Url", redirect);
			requestEvent.getResponse().setHeader("X-Forms_Based_Auth_Dialog_Size", "660x495");	// Default value, added just for reference
		}
		
		return activate;
	}

	@Override
	public boolean handleAuthorization(SecurityConfigurationEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

}
