/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.nio.charset.Charset;

import org.junit.Test;

import com.waveinformatica.skysso.web.exceptions.EncryptionException;
import com.waveinformatica.skysso.web.util.CryptUtils;

public class CryptTest {
	
	@Test
	public void cryptDecryptTest() {
		
		String testStr = "Questa è una stringa di test per testare CryptUtils";
		
		try {
			
			String encrypted = CryptUtils.encryptBase64(testStr.getBytes(Charset.forName("UTF-8")));
			
			byte[] bytes = CryptUtils.decryptBase64(encrypted);
			
			String decrypted = new String(bytes, Charset.forName("UTF-8"));
			
			assertEquals("Wrong decrypted string", testStr, decrypted);
			
		} catch (EncryptionException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
	}
	
}
