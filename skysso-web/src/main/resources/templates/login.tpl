<!DOCTYPE html>
<html>
    <head>
        <base href="${_chain.current.parent.contextPath}/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>${_chain.current.parent.configuration.siteProperties['site-title']!"Ocean"}<#if (extras["page-title"])??> - ${extras["page-title"]}</#if></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="ris/libs/bootstrap-3.3.6/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="ris/libs/font-awesome-4.7.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="ris/libs/AdminLTE-2.3.0/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="ris/libs/AdminLTE-2.3.0/plugins/iCheck/square/blue.css">
		
		<@head scripts=false css=true />
		
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="">${configuration.siteProperties['site-title']!"Ocean"}</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="auth/login" method="post">
                    <div class="form-group has-feedback<#if data.errors?? && (data.errors?seq_contains("authentication.error") || data.errors?seq_contains("authentication.user.notFound") || data.errors?seq_contains("authentication.user.locked"))> has-error</#if>">
                        <#switch extras['loginPageConfig'].userNameType!"TEXT">
                            <#case "EMAIL">
                            <input type="email" class="form-control" placeholder="Email" name="userName">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                <#break>
                            <#case "TEXT">
                            <input type="text" class="form-control" placeholder="User name" name="userName">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                <#break>
                        </#switch>
                        <#if data.errors?? && (data.errors?seq_contains("authentication.error") || data.errors?seq_contains("authentication.user.notFound") || data.errors?seq_contains("authentication.user.locked"))>
                        <span class="help-block"><@i18n key=data.errors[0] /></span>
                        </#if>
                    </div>
                    <div class="form-group has-feedback<#if data.errors?? && (data.errors?seq_contains("authentication.password.wrong") || data.errors?seq_contains("authentication.password.reset") || data.errors?seq_contains("authentication.password.expired"))> has-error</#if>">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <#if data.errors?? && (data.errors?seq_contains("authentication.password.wrong") || data.errors?seq_contains("authentication.password.reset") || data.errors?seq_contains("authentication.password.expired"))>
                        <span class="help-block"><@i18n key=data.errors[0] /></span>
                        </#if>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember" value="true"> Remember Me
                                </label>
                            </div>
                        </div>
                        
                        <input type="hidden" name="redirect" value="${(data.from!contextPath)?html}">
                        
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
					
                    </div>
                </form>
                
                <#if extras['loginPageConfig'].links?has_content>
                <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <#list extras['loginPageConfig'].links as link>
                    <a href="${link.url}" onclick="${link.onClickScript}" class="btn btn-block btn-social ${link.buttonClass} btn-flat"><i class="fa fa-${link.iconClass}"></i> Sign in using ${link.name}</a>
                    </#list>
                </div>
                <!-- /.social-auth-links -->
                </#if>

                <#if extras['loginPageConfig'].forgotPasswordOperation?has_content>
                <a href="${extras['loginPageConfig'].forgotPasswordOperation}">I forgot my password</a><br>
                </#if>
                <#if extras['loginPageConfig'].registerOperation?has_content>
                <a href="${extras['loginPageConfig'].registerOperation}" class="text-center">Register a new membership</a>
                </#if>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="ris/libs/AdminLTE-2.3.0/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="ris/libs/bootstrap-3.3.6/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="ris/libs/AdminLTE-2.3.0/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
		<@head scripts=true css=false />
    </body>
</html>
