/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.base;

import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.Domain;

/**
 *
 * @author Ivano
 */
public interface IUsersManager {
	
	/**
	 * Validates logged in user and returns a {@link OceanUser} for session. If it returns
	 * null, another UsersManager will be invoked if available or login will be
	 * cancelled.
	 * 
	 * @param user the authenticated user
	 * @return a {@link OceanUser} to be put in session
	 */
	public OceanUser validateUser(AuthenticatedUser user);
	
	/**
	 * Returns a operation url for allowing users to restore their password or null
	 * if not available.
	 * 
	 * @return the forgot password operation name
	 */
	public String getForgotPasswordOperation();
	
	/**
	 * Returns a operation url for allowing users to register to the application or
	 * null if not available.
	 * 
	 * @return the registration operation name
	 */
	public String getRegisterOperation();
	
}
