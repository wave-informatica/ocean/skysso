/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.base;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;
import java.util.List;

/**
 *
 * @author Ivano
 */
public interface IBackendAuthService {
	
	/**
	 * Returns if the backend is fully configured and operational
	 * 
         * @param service the service which is invoking this BackendAuthService
         * @param domain the domain to use for checking configuration
	 * @return <code>true</code> if fully configured and operational
	 */
	public boolean isConfigured(SkySSOService service, Domain domain);
	
	/**
	 * Tries to login the user and returns a {@link LoginResponse} with the
	 * result of the operation.
	 * 
         * @param service the service which is invoking this BackendAuthService
	 * @param request the {@link LoginRequest} for login operation
	 * @return the {@link LoginResponse}
	 */
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request);
	
	/**
	 * Returns a {@link UserNameType} for configuring login form validation or null
	 * if this {@link IBackendAuthService} does not support local users login.
	 * 
	 * @return the {@link UserNameType} for login form validation
	 */
	public UserNameType getUserNameType();
	
	/**
	 * Returns a list of {@link AuthenticationLink} corresponding to supported
	 * external authentication services (like OAuth2 services)
	 * 
	 * @return a list of external authentication links
	 */
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result);
	
}
