/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.web.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.web.exceptions.EncryptionException;

public class CryptUtils {
	
	private static final SecretKey secretKey;
	
	static {
		
		SecretKey tmpKey = null;
		try {
			
			KeyGenerator generator = KeyGenerator.getInstance("AES");
			generator.init(128);
			tmpKey = generator.generateKey();
			
		} catch (Exception e) {
			e.printStackTrace();
			LoggerUtils.getLogger(CryptUtils.class).log(Level.SEVERE, "Unable to create secret key", e);
		}
		secretKey = tmpKey;
	}
	
	public static byte[] encrypt(byte[] bytes) throws EncryptionException {
		try {
			
			Cipher c = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			c.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] encrypted = c.doFinal(bytes);

			return encrypted;
			
		} catch (InvalidKeyException e) {
			throw new EncryptionException(e);
		} catch (IllegalBlockSizeException e) {
			throw new EncryptionException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException(e);
		} catch (NoSuchPaddingException e) {
			throw new EncryptionException(e);
		} catch (BadPaddingException e) {
			throw new EncryptionException(e);
		}
	}
	
	public static byte[] decrypt(byte[] bytes) throws EncryptionException {

		try {
			
			Cipher c = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			c.init(Cipher.DECRYPT_MODE, secretKey);
			return c.doFinal(bytes);
			
		} catch (InvalidKeyException e) {
			throw new EncryptionException(e);
		} catch (IllegalBlockSizeException e) {
			throw new EncryptionException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException(e);
		} catch (NoSuchPaddingException e) {
			throw new EncryptionException(e);
		} catch (BadPaddingException e) {
			throw new EncryptionException(e);
		}
	}
	
	public static String encryptBase64(byte[] bytes) throws EncryptionException {
		return Base64.encodeBase64String(encrypt(bytes));
	}
	
	public static byte[] decryptBase64(String soruce) throws EncryptionException {
		return decrypt(Base64.decodeBase64(soruce));
	}
	
}
