/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.web.base;

import java.nio.charset.Charset;

import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.events.LoginEvent;
import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.FrontendAuthService;
import com.waveinformatica.skysso.web.dto.Domain;

@FrontendAuthService(priority = 5)
public class BasicAuthFrontend implements IFrontendAuthService {
	
	@Inject
	private CoreController controller;
	
	@Inject
	private UserSession userSession;
	
	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean handleRequest(Domain domain, WebRequestEvent requestEvent, SecurityConfigurationEvent evt, String opName) {
		
		String userAgent = requestEvent.getRequest().getHeader("User-Agent");
		if (StringUtils.isNotBlank(userAgent) && userAgent.startsWith("Mozilla")) {
			return false;
		}
		
		requestEvent.getResponse().setStatus(401);
		requestEvent.getResponse().addHeader("WWW-Authenticate", "Basic realm=\"" + Application.getInstance().getNamespace() + "@" + domain.getName() + "\", charset=\"UTF-8\"");
		
		return true;
	}

	@Override
	public boolean handleAuthorization(SecurityConfigurationEvent event) {
		
		String authorization = Context.get().getRequest().getHeader("Authorization");
		
		if (!authorization.toLowerCase().startsWith("basic ")) {
			return false;
		}
		
		String credentials = new String(Base64.decodeBase64(authorization.substring("Basic ".length())), Charset.forName("UTF-8"));
		
		int colon = credentials.indexOf(':');
		
		LoginEvent login = new LoginEvent(this, credentials.substring(0, colon), credentials.substring(colon + 1), Context.get().getRequest(), null);
		controller.dispatchEvent(login);
		
		if (login.isManaged() && login.getLoggedUser() != null) {

			userSession.login(login.getLoggedUser());
			
		}
		
		return true;
	}

}
