/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Ivano
 */
@Entity
@Table(name = "skysso_user_app_domain_authz")
public class UserAppDomainAuthorization implements Serializable {

	@Embeddable
	public static class Key implements Serializable {

		@Column(name = "user_id")
		private Long userId;

		@Column(length = 50)
		private String domain;

		@Column(length = 50)
		private String appName;

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public String getAppName() {
			return appName;
		}

		public void setAppName(String appName) {
			this.appName = appName;
		}

	}

	@EmbeddedId
	private Key key = new Key();

	private Boolean authorized;

	private String token;

	@Column(name = "token_expiration")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tokenEspiration;

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public Boolean getAuthorized() {
		return authorized;
	}

	public void setAuthorized(Boolean authorized) {
		this.authorized = authorized;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getTokenEspiration() {
		return tokenEspiration;
	}

	public void setTokenEspiration(Date tokenEspiration) {
		this.tokenEspiration = tokenEspiration;
	}

	public Long getUserId() {
		return key.getUserId();
	}

	public void setUserId(Long userId) {
		key.setUserId(userId);
	}

	public String getDomain() {
		return key.getDomain();
	}

	public void setDomain(String domain) {
		key.setDomain(domain);
	}

	public String getAppName() {
		return key.getAppName();
	}

	public void setAppName(String appName) {
		key.setAppName(appName);
	}

}
