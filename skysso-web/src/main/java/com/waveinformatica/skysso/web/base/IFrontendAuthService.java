/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.web.base;

import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.Domain;

public interface IFrontendAuthService {
	
	public boolean isConfigured(SkySSOService service, Domain domain);
	
	public boolean handleRequest(Domain domain, WebRequestEvent requestEvent, SecurityConfigurationEvent evt, String opName);
	
	public boolean handleAuthorization(SecurityConfigurationEvent event);
	
}
