/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.events;

import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ivano
 */
public class UserAuthenticatedEvent extends Event {
	
	public static final String NAME = "SKYSSO_USER_AUTHENTICATED";
	
	private final HttpServletRequest request;
	private final HttpServletResponse response;
	private final AuthenticatedUser authUser;
	private final OceanUser user;

	public UserAuthenticatedEvent(Object source, HttpServletRequest request, HttpServletResponse response, AuthenticatedUser authUser, OceanUser user) {
		super(source, NAME);
		this.request = request;
		this.response = response;
		this.authUser = authUser;
		this.user = user;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public AuthenticatedUser getAuthUser() {
		return authUser;
	}

	public OceanUser getUser() {
		return user;
	}
	
}
