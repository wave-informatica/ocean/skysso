/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.web;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.skysso.web.dto.Application;
import com.waveinformatica.skysso.web.dto.Domain;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "lookup")
public class LookupOperations {
	
	@Inject
	private CoreController controller;
	
	@Operation(value = "user", defaultOutputType = MimeTypes.JSON)
	@SkipAuthorization
	public UserResult lookupUser(@Param("token") String token, HttpServletRequest request) {
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		Domain domain = service.getDomain(request);
		
		OceanUser result = service.getUserInfo(domain, token);
		
		Application app = service.getApplicationInfo(domain, token);
		
		return new UserResult(result, app);
		
	}
	
	public static class UserResult extends BaseResult {
		
		private OceanUser user;
		private Application application;
		
		public UserResult() {
		}
		
		public UserResult(OceanUser user, Application application) {
			this.user = user;
			this.application = application;
		}
		
		public OceanUser getUser() {
			return user;
		}
		
		public void setUser(OceanUser user) {
			this.user = user;
		}

		public Application getApplication() {
			return application;
		}

		public void setApplication(Application application) {
			this.application = application;
		}
		
	}
	
}
