/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.web;

import com.waveinformatica.skysso.web.dto.LoginPageConfig;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.BeforeResultEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.IUsersManager;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@CoreEventListener(eventNames = CoreEventName.BEFORE_RESULT, flags = "com.waveinformatica.ocean.core.controllers.results.WebPageResult")
public class LoginPageConfigProvider implements IEventListener {
	
	@Inject
	private CoreController controller;
	
	@Override
	public void performAction(Event event) {
		
		BeforeResultEvent evt = (BeforeResultEvent) event;
		
		if (evt.getOperationInfo() == null || (
			  !evt.getOperationInfo().getFullName().equals("/auth/loginPage") &&
			  !evt.getOperationInfo().getFullName().equals("/auth/login"))) {
			return;
		}
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		LoginPageConfig config = new LoginPageConfig();
		
		Domain domain = service.getDomain(evt.getRequest());
		
		for (IBackendAuthService bas : service.getAvailableBackends()) {
			if (!bas.isConfigured(service, domain)) {
				continue;
			}
			if (config.getUserNameType() == null) {
				config.setUserNameType(bas.getUserNameType());
			}
			List<AuthenticationLink> links = bas.getExternalAuthenticationLinks(evt.getResult());
			if (links != null && !links.isEmpty()) {
				config.getLinks().addAll(links);
			}
		}
		
		for (IUsersManager um : service.getAvailableUsersManagers()) {
			if (config.getForgotPasswordOperation() == null) {
				config.setForgotPasswordOperation(um.getForgotPasswordOperation());
			}
			if (config.getRegisterOperation() == null) {
				config.setRegisterOperation(um.getRegisterOperation());
			}
		}
		
		evt.getResult().putExtra("loginPageConfig", config);
		
	}
	
}
