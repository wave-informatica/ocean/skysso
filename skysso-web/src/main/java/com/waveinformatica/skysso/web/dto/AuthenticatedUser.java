/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.web.dto;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.security.OceanUser;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ivano
 */
public class AuthenticatedUser implements OceanUser {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String userId;
	private String fullName;
	private String authBackend;
	@ExcludeSerialization
	private final Map<String, Object> fields = new HashMap<String, Object>();
	private Date authDate;
	private Domain domain;
	
	public AuthenticatedUser() {
	}
	
	public AuthenticatedUser(String userId, String fullName) {
		this.userId = userId;
		this.fullName = fullName;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@Override
	public String toString() {
		return fullName + " (" + userId + ")";
	}
	
	public String getAuthBackend() {
		return authBackend;
	}
	
	public void setAuthBackend(String authBackend) {
		this.authBackend = authBackend;
	}
	
	public Map<String, Object> getFields() {
		return fields;
	}
	
	public Date getAuthDate() {
		return authDate;
	}
	
	public void setAuthDate(Date authDate) {
		this.authDate = authDate;
	}
	
	public Domain getDomain() {
		return domain;
	}
	
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	
}
