/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.web;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.Preferred;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.LoginEvent;
import com.waveinformatica.ocean.core.controllers.events.LogoutEvent;
import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.modules.ComponentsBin;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.web.annotations.ApplicationsManager;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.annotations.DomainsManager;
import com.waveinformatica.skysso.web.annotations.FrontendAuthService;
import com.waveinformatica.skysso.web.annotations.UsersManager;
import com.waveinformatica.skysso.web.base.IApplicationsManager;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.IDomainsManager;
import com.waveinformatica.skysso.web.base.IFrontendAuthService;
import com.waveinformatica.skysso.web.base.IUsersManager;
import com.waveinformatica.skysso.web.dao.AuthorizationsDao;
import com.waveinformatica.skysso.web.dto.Application;
import com.waveinformatica.skysso.web.dto.AuthResultCode;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;
import com.waveinformatica.skysso.web.entities.TokenEntity;
import com.waveinformatica.skysso.web.events.UserAuthenticatedEvent;
import com.waveinformatica.skysso.web.exceptions.EncryptionException;
import com.waveinformatica.skysso.web.util.CryptUtils;

/**
 *
 * @author Ivano
 */
@Service("SkySSO")
public class SkySSOService implements IService {
	
	@Inject
	@Preferred
	@FrontendAuthService
	@BackendAuthService
	@UsersManager
	@ApplicationsManager
	@DomainsManager
	private ComponentsBin components;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private Configuration configuration;
	
	public Domain getDomain(HttpServletRequest request) {
		
		List<Class> classes = new ArrayList<Class>();
		classes.addAll(components.getComponents(DomainsManager.class));
		
		Collections.sort(classes, new Comparator<Class>() {
			@Override
			public int compare(Class t, Class t1) {
				DomainsManager a = (DomainsManager) t.getAnnotation(DomainsManager.class);
				DomainsManager a1 = (DomainsManager) t1.getAnnotation(DomainsManager.class);
				return a.priority() - a1.priority();
			}
		});
		
		for (Class<? extends IDomainsManager> c : classes) {
			IDomainsManager manager = factory.newInstance(c);
			Domain d = manager.getRequestDomain(request);
			if (d != null) {
				d.setManagerName(manager.getClass().getAnnotation(DomainsManager.class).value());
				return d;
			}
		}
		
		return null;
		
	}
	
	public LoginResponse tryLogin(LoginRequest login) {
		
		LoginResponse response = null;
		
		List<Class<? extends IBackendAuthService>> services = new ArrayList<Class<? extends IBackendAuthService>>((Collection) components.getComponents(BackendAuthService.class));
		Collections.sort(services, new Comparator<Class<? extends IBackendAuthService>>() {
			@Override
			public int compare(Class<? extends IBackendAuthService> o1, Class<? extends IBackendAuthService> o2) {
				return o2.getAnnotation(BackendAuthService.class).priority() - o1.getAnnotation(BackendAuthService.class).priority();
			}
		});
		
		for (Class<? extends IBackendAuthService> c : services) {
			IBackendAuthService authService = factory.newInstance(c);
			
			if (!authService.isConfigured(this, login.getDomain())) {
				continue;
			}
			
			LoginResponse localResponse = null;
			try {
				localResponse = authService.tryUserLogin(this, login);
			} catch (Exception e) {
				LoggerUtils.getLogger(SkySSOService.class).log(Level.SEVERE, "Error invoking Authentication Backend Service \"" + c.getName() + "\"", e);
				continue;
			}
			
			if (localResponse == null) {
				LoggerUtils.getLogger(SkySSOService.class).log(Level.SEVERE, "BUG: Authentication Backend Service \"" + c.getName() + "\" returned NULL response");
				continue;
			}
			else if (localResponse.getResultCode() == null) {
				LoggerUtils.getLogger(SkySSOService.class).log(Level.SEVERE, "BUG: Authentication Backend Service \"" + c.getName() + "\" returned NULL result code");
				continue;
			}
			else if (localResponse.getResultCode() == AuthResultCode.ERROR) {
				LoggerUtils.getLogger(SkySSOService.class).log(Level.WARNING, "Authentication Backend Service \"" + c.getName() + "\" returned ERROR");
				if (response == null) {
					response = localResponse;
				}
				continue;
			}
			
			localResponse.setBackendService(authService);
			
			if (localResponse.getResultCode() == AuthResultCode.OK) {
				localResponse.getUser().setDomain(login.getDomain());
				return localResponse;
			}
			else if (response == null || localResponse.getResultCode().ordinal() < response.getResultCode().ordinal()) {
				response = localResponse;
			}
		}
		
		return response;
		
	}
	
	public OceanUser validateUser(AuthenticatedUser user) {
		
		Set<Class<? extends IUsersManager>> usersManagers = (Set) components.getComponents(UsersManager.class);
		if (usersManagers.isEmpty()) {
			return user;
		}
		
		List<Class<? extends IUsersManager>> orderedManagers = new ArrayList<Class<? extends IUsersManager>>(usersManagers);
		Collections.sort(orderedManagers, new Comparator<Class<? extends IUsersManager>>() {
			@Override
			public int compare(Class<? extends IUsersManager> o1, Class<? extends IUsersManager> o2) {
				UsersManager m1 = o1.getAnnotation(UsersManager.class);
				UsersManager m2 = o2.getAnnotation(UsersManager.class);
				return m2.priority() - m1.priority();
			}
		});
		
		for (Class<? extends IUsersManager> managerCls : orderedManagers) {
			IUsersManager manager = factory.newInstance(managerCls);
			try {
				OceanUser result = manager.validateUser(user);
				if (result != null) {
					return result;
				}
			} catch (Throwable t) {
				LoggerUtils.getLogger(SkySSOService.class).log(Level.SEVERE, "Error invoking validateUser method on UserManager of type " + managerCls.getName() + ". Skipped...", t);
			}
		}
		
		return null;
		
	}
	
	public Application getDomainApplication(HttpServletRequest request, String appName) {
		
		// Get domain from request
		Domain domain = getDomain(request);
		
		if (domain == null) {
			return null;
		}
		
		// Get authorized application from domain and appName
		for (Class<?> c : components.getComponents(ApplicationsManager.class)) {
			
			IApplicationsManager manager = (IApplicationsManager) factory.newInstance(c);
			
			Application app = manager.getApplication(domain, appName);
			
			if (app != null) {
				app.setDomain(domain);
				app.setCallbackUrl(request.getParameter("redirect_uri"));
				
				return app;
			}
		}
		
		return null;
		
	}
	
	public List<Application> getDomainApplications(HttpServletRequest request) {
		
		// Get domain from request
		Domain domain = getDomain(request);
		
		if (domain == null) {
			return null;
		}
		
		List<Application> apps = new ArrayList<Application>();
		
		// Get authorized application from domain and appName
		for (Class<?> c : components.getComponents(ApplicationsManager.class)) {
			
			IApplicationsManager manager = (IApplicationsManager) factory.newInstance(c);
			
			apps.addAll(manager.getDomainApplications(domain));
			
		}
		
		for (Application app : apps) {
			app.setDomain(domain);
		}
		
		return apps;
		
	}
	
	public Boolean isApplicationAuthorized(Application app) {
		
		com.waveinformatica.ocean.core.security.SecurityManager sm = factory.getBean(com.waveinformatica.ocean.core.security.SecurityManager.class);
		
		return factory.newInstance(AuthorizationsDao.class)
			    .getDomainApplicationAuthorization(sm.getPrincipal().getId(), app.getSysName(), app.getDomain().getName())
			    .getAuthorized();
		
	}
	
	public void setApplicationAuthorization(Application app, Boolean authorized) {
		
		UserSession session = factory.getBean(UserSession.class);
		
		factory.newInstance(AuthorizationsDao.class)
			    .setApplicationDomainAuthorization(session.getPrincipal().getId(), app.getSysName(), app.getDomain().getName(), authorized);
		
	}
	
	public boolean checkApplicationSecret(Application app, String secret) {
		
		// Get authorized application from domain and appName
		for (Class<?> c : components.getComponents(ApplicationsManager.class)) {
			
			IApplicationsManager manager = (IApplicationsManager) factory.newInstance(c);
			
			Application vApp = manager.getApplication(app.getDomain(), app.getSysName());
			
			if (vApp != null) {
				vApp.setDomain(app.getDomain());
				
				return manager.verifyApplicationSecret(vApp, secret);
			}
		}
		
		return false;
	}
	
	public Date storeApplicationToken(Application app, Long userId, String token, String oldToken) {
		
		AuthorizationsDao dao = factory.newInstance(AuthorizationsDao.class);
		
		TokenEntity entity = dao.saveToken(app, userId, token);
		
		return entity.getExpires();
		
	}
	
	public OceanUser getUserInfo(Domain domain, String token) {
		
		AuthorizationsDao dao = factory.newInstance(AuthorizationsDao.class);
		
		TokenEntity entity = dao.getTokenObject(domain.getName(), token);
		
		if (entity == null || !entity.getDomainName().equals(domain.getName())) {
			return null;
		}
		
		AuthenticatedUser user = new AuthenticatedUser();
		user.setId(entity.getUserId());
		user.setDomain(domain);
		
		for (Class<?> c : components.getComponents(UsersManager.class)) {
			IUsersManager manager = (IUsersManager) factory.newInstance(c);
			OceanUser finalUser = manager.validateUser(user);
			if (finalUser != null) {
				return finalUser;
			}
		}
		
		return null;
		
	}
	
	public Application getApplicationInfo(Domain domain, String token) {
		
		AuthorizationsDao dao = factory.newInstance(AuthorizationsDao.class);
		
		TokenEntity entity = dao.getTokenObject(domain.getName(), token);
		
		if (entity == null || !entity.getDomainName().equals(domain.getName())) {
			return null;
		}
		
		for (Class<?> c : components.getComponents(ApplicationsManager.class)) {
			IApplicationsManager manager = (IApplicationsManager) factory.newInstance(c);
			Application app = manager.getApplication(domain, entity.getAppName());
			if (app != null) {
				return app;
			}
		}
		
		return null;
		
	}
	
	public List<IUsersManager> getAvailableUsersManagers() {
		List<IUsersManager> result = new ArrayList<IUsersManager>();
		for (Class<?> c : components.getComponents(UsersManager.class)) {
			result.add((IUsersManager) factory.newInstance(c));
		}
		return result;
	}
	
	public List<IBackendAuthService> getAvailableBackends() {
		List<IBackendAuthService> result = new ArrayList<IBackendAuthService>();
		for (Class<?> c : components.getComponents(BackendAuthService.class)) {
			result.add((IBackendAuthService) factory.newInstance(c));
		}
		return result;
	}
	
	public List<IFrontendAuthService> getAvailableFrontends() {
		List<Class> classes = new ArrayList<Class>(components.getComponents(FrontendAuthService.class));
		Collections.sort(classes, new Comparator<Class>() {
			@Override
			public int compare(Class o1, Class o2) {
				FrontendAuthService ann1 = (FrontendAuthService) o1.getAnnotation(FrontendAuthService.class);
				FrontendAuthService ann2 = (FrontendAuthService) o2.getAnnotation(FrontendAuthService.class);
				return ann2.priority() - ann1.priority();
			}
		});
		
		List<IFrontendAuthService> result = new ArrayList<IFrontendAuthService>();
		for (Class<? extends IFrontendAuthService> c : classes) {
			result.add(factory.newInstance(c));
		}
		return result;
	}
	
	public Cookie createAuthCookie(AuthenticatedUser user, int days) {
		
		String cookieValue = JsonUtils.getBuilder().create().toJson(user);
		
		try {
			cookieValue = CryptUtils.encryptBase64(cookieValue.getBytes(Charset.forName("UTF-8")));
		} catch (EncryptionException ex) {
			return null;
		}
		
		String path = Context.get().getServletContext().getContextPath();
		if (!path.endsWith("/")) {
			path += "/";
		}
		Cookie cookie = new Cookie("skysso.auth", cookieValue);
		cookie.setMaxAge(days * 24 * 3600);
		cookie.setPath(path);
		
		return cookie;
		
	}
	
	public AuthenticatedUser getUserFromCookie(HttpServletRequest request, int days) {
		
		String json = null;
		
		if (StringUtils.isNotBlank(request.getParameter("skysso.auth"))) {
			json = request.getParameter("skysso.auth");
		}
		else {
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {

					if (!cookie.getName().equals("skysso.auth")) {
						continue;
					}

					json = cookie.getValue();
					
					break;
					
				}
			}
		}
		
		if (StringUtils.isNotBlank(json)) {

			try {
				json = new String(CryptUtils.decryptBase64(json), Charset.forName("UTF-8"));
			} catch (EncryptionException ex) {
				return null;
			}

			AuthenticatedUser user = JsonUtils.getBuilder().create().fromJson(json, AuthenticatedUser.class);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -days);
			if (cal.getTime().after(user.getAuthDate())) {
				return null;
			}

			return user;
			
		}
		
		return null;
		
	}
	
	public Map<String, String> getDomainConfiguration(Domain domain, String backend) {
		for (Class<?> c : components.getComponents(DomainsManager.class)) {
			if (c.getAnnotation(DomainsManager.class).value().equals(domain.getManagerName())) {
				IDomainsManager manager = (IDomainsManager) factory.newInstance(c);
				return manager.getAuthConfiguration(domain, backend);
			}
		}
		return null;
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus status = new ServiceStatus();
		status.setRunning(true);
		return status;
	}
	
	@CoreEventListener(eventNames = CoreEventName.SECURITY_CONFIGURATION)
	public static class SecurityConfigurator implements IEventListener<SecurityConfigurationEvent> {
		
		@Inject
		private CoreController controller;
		
		@Inject
		private ObjectFactory factory;
		
		@Inject
		private UserSession session;
		
		@Inject
		private SecurityManager sm;
		
		@Override
		public void performAction(SecurityConfigurationEvent evt) {
			
			SkySSOService service = controller.getService(SkySSOService.class);
			
			Set<Class<?>> backendServices = service.components.getComponents(BackendAuthService.class);
			Set<Class<?>> domainsServices = service.components.getComponents(DomainsManager.class);
			Set<Class<?>> usersManagers = service.components.getComponents(UsersManager.class);
			
			boolean backendAvailable = false;
			Domain domain = null;
			if (!domainsServices.isEmpty()) {
				domain = service.getDomain(Context.get().getRequest());
				for (Class<?> backendCls : backendServices) {
					IBackendAuthService backend = (IBackendAuthService) factory.newInstance(backendCls);
					backendAvailable = backendAvailable || backend.isConfigured(service, domain);
				}
			}
			
			// TODO: fare piu' controlli se necessario
			evt.setAuthenticationNeeded(
				    backendAvailable &&
						  !domainsServices.isEmpty() &&
						  !usersManagers.isEmpty()
			);
			
			if (evt.isAuthenticationNeeded() && session.isLogged()) {
				ClassLoader userClassLoader = session.getPrincipal().getClass().getClassLoader();
				if (userClassLoader instanceof ModuleClassLoader) {
					if (!((ModuleClassLoader) userClassLoader).getModule().isLoaded() ||
						    userClassLoader != ((ModuleClassLoader) userClassLoader).getModule().getModuleClassLoader()) {
						session.logout();
					}
				}
			}
			
			if (evt.isAuthenticationNeeded() && !session.isLogged() && Context.get().getRequest() != null) {
				AuthenticatedUser user = service.getUserFromCookie(Context.get().getRequest(), 30);
				if (user != null) {
					OceanUser finalUser = service.validateUser(user);
					if (finalUser != null) {
						session.login(finalUser);
					}
				}
				if (!sm.isLogged()) {
					String authzValue = Context.get().getRequest().getHeader("Authorization");
					if (StringUtils.isNotBlank(authzValue)) {
						for (IFrontendAuthService ifas : service.getAvailableFrontends()) {
							try {
								if (ifas.handleAuthorization(evt)) {
									break;
								}
							} catch (Throwable t) {
								LoggerUtils.getLogger(ifas.getClass()).log(Level.SEVERE, "The @FrontendAuthService " + ifas.getClass().getName() + " throwed an exception during an authentication operation. Going ahead.", t);
							}
						}
					}
					if (!sm.isLogged()) {
						SkySSOAuthenticationHandler handler = factory.newInstance(SkySSOAuthenticationHandler.class);
						handler.setDomain(domain);
						handler.setOldAuthHandler(evt.getHandler());
						evt.setHandler(handler);
					}
				}
			}
			
		}
		
	}
	
	@CoreEventListener(eventNames = CoreEventName.LOGIN)
	public static class LoginController implements IEventListener<LoginEvent> {
		
		@Inject
		private CoreController controller;
		
		@Override
		public void performAction(LoginEvent evt) {
			
			SkySSOService service = controller.getService(SkySSOService.class);
			
			Domain domain = service.getDomain(evt.getRequest());
			
			LoginRequest request = new LoginRequest();
			request.setUserName(evt.getUserName());
			request.setPassword(evt.getPassword());
			request.setDomain(domain);
			
			LoginResponse response = service.tryLogin(request);
			
			if (response == null) {
				return;
			}
			
			if (response.getResultCode() == AuthResultCode.OK) {
				
				AuthenticatedUser user = response.getUser();
				BackendAuthService annotation = response.getBackendService().getClass().getAnnotation(BackendAuthService.class);
				if (annotation.value().length() > 0) {
					user.setAuthBackend(annotation.value());
				}
				user.setAuthDate(new Date());
				
				// Retrieve final session user
				OceanUser finalUser = service.validateUser(user);
				if (finalUser == null) {
					evt.getErrors().add("authentication.user.notFound");
				}
				else {
					
					evt.setLoggedUser(finalUser);
					
					if (evt.getRequest() != null && "true".equals(evt.getRequest().getParameter("remember"))) {
						Cookie cookie = service.createAuthCookie(user, 30);
						if (cookie != null) {
							evt.getResponse().addCookie(cookie);
						}
					}
					
					UserAuthenticatedEvent userAuthEvent = new UserAuthenticatedEvent(this, evt.getRequest(), evt.getResponse(), user, finalUser);
					controller.dispatchEvent(userAuthEvent);
					
				}
				
			}
			else {
				switch (response.getResultCode()) {
					case USER_ACCOUNT_LOCKED:
						evt.getErrors().add("authentication.user.locked");
						break;
					case EXPIRED_PASSWORD:
						evt.getErrors().add("authentication.password.expired");
						break;
					case RESET_PASSWORD:
						evt.getErrors().add("authentication.password.reset");
						break;
					case USER_NOT_FOUND:
						evt.getErrors().add("authentication.user.notFound");
						break;
					case WRONG_PASSWORD:
						evt.getErrors().add("authentication.password.wrong");
						break;
					case ERROR:
						evt.getErrors().add("authentication.error");
						break;
				}
			}
			
			evt.setManaged(true);
			
		}
		
	}
	
	@CoreEventListener(eventNames = CoreEventName.LOGOUT)
	public static class LogoutListener implements IEventListener<LogoutEvent> {
		
		@Override
		public void performAction(LogoutEvent evt) {
			
			Cookie[] cookies = evt.getRequest().getCookies();
			for (Cookie cookie : cookies) {
				if ("skysso.auth".equals(cookie.getName())) {
					String path = Context.get().getServletContext().getContextPath();
					if (!path.endsWith("/")) {
						path += "/";
					}
					cookie.setPath(path);
					cookie.setMaxAge(0);
					evt.getResponse().addCookie(cookie);
					break;
				}
			}
			
		}
		
	}
	
}
