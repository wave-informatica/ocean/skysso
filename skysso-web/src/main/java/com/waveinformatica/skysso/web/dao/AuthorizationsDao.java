/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.dao;

import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.web.dto.Application;
import com.waveinformatica.skysso.web.entities.TokenEntity;
import com.waveinformatica.skysso.web.entities.UserAppDomainAuthorization;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 *
 * @author Ivano
 */
public class AuthorizationsDao {
    
    @Inject
    @OceanPersistenceContext
    private EntityManager em;
    
    public UserAppDomainAuthorization getDomainApplicationAuthorization(Long userId, String appName, String domain) {
	
	UserAppDomainAuthorization.Key key = new UserAppDomainAuthorization.Key();
	key.setAppName(appName);
	key.setDomain(domain);
	key.setUserId(userId);
	
	UserAppDomainAuthorization auth = em.find(UserAppDomainAuthorization.class, key);
	
	if (auth == null) {
	    auth = new UserAppDomainAuthorization();
	    auth.setAppName(appName);
	    auth.setDomain(domain);
	    auth.setUserId(userId);
	    auth.setAuthorized(null);
	}
	
	return auth;
	
    }
    
    public void setApplicationDomainAuthorization(Long userId, String appName, String domain, Boolean authorize) {
	
	UserAppDomainAuthorization auth = getDomainApplicationAuthorization(userId, appName, domain);
	
	try {
	    
	    em.getTransaction().begin();
	    
	    if (authorize == null && auth.getAuthorized() != null) {
		em.remove(auth);
	    }
	    else if (authorize != null && auth.getAuthorized() == null) {
		auth.setAuthorized(authorize);
		em.persist(auth);
	    }
	    else if (authorize != null && auth.getAuthorized() != null) {
		auth.setAuthorized(authorize);
		em.merge(auth);
	    }
	    
	    em.getTransaction().commit();
	    
	} catch (Exception e) {
	    if (em.getTransaction().isActive()) {
		em.getTransaction().rollback();
	    }
	    LoggerUtils.getLogger(AuthorizationsDao.class).log(Level.SEVERE, "Error saving authorization", e);
	}
	
    }
    
    public TokenEntity saveToken(Application app, Long user, String token) {
        
        try {
            
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DAY_OF_MONTH, 30);
            
            em.getTransaction().begin();
            
            TokenEntity entity = new TokenEntity();
            entity.setAppName(app.getSysName());
            entity.setDomainName(app.getDomain().getName());
            entity.setExpires(cal.getTime());
            entity.setUserId(user);
            entity.setToken(token);
            
            em.persist(entity);
            
            em.getTransaction().commit();
            
            return entity;
            
        } catch (Exception e) {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            LoggerUtils.getLogger(AuthorizationsDao.class).log(Level.SEVERE, "Error storing token", e);
        }
        
        return null;
        
    }
    
    public TokenEntity getTokenObject(String domainName, String token) {
        
        try {
            
            TokenEntity entity = em.createQuery("select x from TokenEntity x where x.domainName = :domain and x.token = :token", TokenEntity.class)
                    .setParameter("domain", domainName)
                    .setParameter("token", token)
                    .getSingleResult();
		
		if (entity.getExpires().before(new Date())) {
			
			boolean privateTransaction = !em.getTransaction().isActive();
			
			try {
				if (privateTransaction) {
					em.getTransaction().begin();
				}
				
				em.remove(entity);
				
				if (privateTransaction) {
					em.getTransaction().commit();
				}
				
				entity = null;
				
			} catch (Exception e) {
				if (privateTransaction && em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			}
		}
		
		return entity;
            
        } catch (NoResultException e) {
            return null;
        }
        
    }
    
}
