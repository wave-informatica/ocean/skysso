/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.dto;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;

/**
 *
 * @author Ivano
 * @author Luca Lattore
 */
public class AuthenticationLink {
	
	private final String url;
	private final String buttonClass;
	private final String iconClass;
	private final String name;
	private final String onClickScript;

	public AuthenticationLink(String url, String buttonClass, String iconClass, String name, String onClickScript) {
		this.url = url;
		this.buttonClass = buttonClass;
		this.iconClass = iconClass;
		this.name = name;
		this.onClickScript = onClickScript;
	}
	
	public AuthenticationLink(String url, String buttonClass, String iconClass, String name) {
		this(url, buttonClass, iconClass, name, "");
	}

	public String getUrl() {
		return url;
	}

	public String getButtonClass() {
		return buttonClass;
	}

	public String getIconClass() {
		return iconClass;
	}

	public String getName() {
		return name;
	}
	
	public String getOnClickScript() {
		return onClickScript;
	}
	
}
