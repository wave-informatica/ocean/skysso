/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.dto;

import com.waveinformatica.skysso.web.base.IBackendAuthService;
import java.util.List;

/**
 *
 * @author Ivano
 */
public class LoginResponse {
	
	private AuthResultCode resultCode;
	private AuthenticatedUser user;
	private IBackendAuthService backendService;

	public LoginResponse() {
	}

	public LoginResponse(AuthResultCode resultCode) {
		this.resultCode = resultCode;
	}

	public LoginResponse(AuthenticatedUser user) {
		this.user = user;
		resultCode = AuthResultCode.OK;
	}
	
	public AuthResultCode getResultCode() {
		return resultCode;
	}

	public void setResultCode(AuthResultCode resultCode) {
		this.resultCode = resultCode;
	}

	public AuthenticatedUser getUser() {
		return user;
	}

	public void setUser(AuthenticatedUser user) {
		this.user = user;
	}

	public IBackendAuthService getBackendService() {
		return backendService;
	}

	public void setBackendService(IBackendAuthService backendService) {
		this.backendService = backendService;
	}
	
}
