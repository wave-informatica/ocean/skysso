/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.web.dto;

import com.waveinformatica.skysso.web.base.UserNameType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivano
 */
public class LoginPageConfig {
	private UserNameType userNameType;
	private String forgotPasswordOperation;
	private String registerOperation;
	private final List<AuthenticationLink> links = new ArrayList<AuthenticationLink>();

	public UserNameType getUserNameType() {
		return userNameType;
	}

	public void setUserNameType(UserNameType userNameType) {
		this.userNameType = userNameType;
	}

	public String getForgotPasswordOperation() {
		return forgotPasswordOperation;
	}

	public void setForgotPasswordOperation(String forgotPasswordOperation) {
		this.forgotPasswordOperation = forgotPasswordOperation;
	}

	public String getRegisterOperation() {
		return registerOperation;
	}

	public void setRegisterOperation(String registerOperation) {
		this.registerOperation = registerOperation;
	}

	public List<AuthenticationLink> getLinks() {
		return links;
	}
	
}
