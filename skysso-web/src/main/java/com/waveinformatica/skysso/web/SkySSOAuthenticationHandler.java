/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.web;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.security.AuthenticationHandler;
import com.waveinformatica.ocean.core.security.OceanAuthenticationHandler;
import com.waveinformatica.skysso.web.base.IFrontendAuthService;
import com.waveinformatica.skysso.web.dto.Domain;

public class SkySSOAuthenticationHandler extends OceanAuthenticationHandler {
	
	@Inject
	private CoreController controller;
	
	private Domain domain;
	private AuthenticationHandler oldAuthHandler;
	
	@Override
	public Object getAuthenticationResult(WebRequestEvent requestEvent, SecurityConfigurationEvent evt, String opName) {
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		boolean handled = false;
		
		for (IFrontendAuthService frontend : service.getAvailableFrontends()) {
			if (frontend.isConfigured(service, domain)) {
				if (frontend.handleRequest(domain, requestEvent, evt, opName)) {
					handled = true;
				}
			}
		}
		
		if (!handled) {
			return super.getAuthenticationResult(requestEvent, evt, opName);
		}
		else {
			return null;
		}
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public AuthenticationHandler getOldAuthHandler() {
		return oldAuthHandler;
	}

	public void setOldAuthHandler(AuthenticationHandler oldAuthHandler) {
		this.oldAuthHandler = oldAuthHandler;
	}

}
