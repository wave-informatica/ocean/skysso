/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.google;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;

/**
 * @author Luca Lattore
 *
 */
@BackendAuthService("GOOGLE")
@OperationProvider(namespace = "sso/google")
public class GoogleLogin implements IBackendAuthService {
	
	private static final Logger logger = LoggerUtils.getLogger(GoogleLogin.class);
	
	@Inject
	private Configuration config;
	
	@Inject
	private CoreController controller;
	
	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager sm;

	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		PersistedProperties props = config.getCustomProperties("skysso.google");
		return StringUtils.isNotBlank(props.getProperty("google.clientid")) && 
				StringUtils.isNotBlank(props.getProperty("google.secret")) &&
				StringUtils.isNotBlank(props.getProperty("google.redirecturi")) &&
				StringUtils.isNotBlank(props.getProperty("google.scope"));
	}
	
	@Override
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {
		return null;
	}

	@Override
	public UserNameType getUserNameType() {
		return null;
	}

	@Override
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
		try {
			PersistedProperties props = config.getCustomProperties("skysso.google");
			String clientId = props.getProperty("google.clientid");
			String redirectUri = URLEncoder.encode(props.getProperty("google.redirecturi"), "UTF-8");
			String scope = URLEncoder.encode(props.getProperty("google.scope"), "UTF-8");
			String state = Context.get().getRequest().getParameter("redirect");
			List<AuthenticationLink> links = new ArrayList<AuthenticationLink>();
			String loginUrl = "https://accounts.google.com/o/oauth2/v2/auth?" + 
					"scope=" + scope + "&" + 
					"include_granted_scopes=true&" + 
					"state=" + URLEncoder.encode(state, "UTF-8") + "&" + 
					"redirect_uri="+ redirectUri + "&" + 
					"response_type=code&access_type=offline&" + 
					"prompt=consent&" +
					"client_id=" + clientId;
			links.add(new AuthenticationLink(loginUrl, "btn-google", "google", "Google"));
			return links;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Operation("login")
	@SkipAuthorization
	public BaseResult googleLoggedIn(@Param("code") String code, HttpServletRequest request) {
		try {
			PersistedProperties props = config.getCustomProperties("skysso.google");
			String clientId = props.getProperty("google.clientid");
			String redirectUri = URLEncoder.encode(props.getProperty("google.redirecturi"), "UTF-8");
			String clientSecret = props.getProperty("google.secret");;
			
			String url = "https://www.googleapis.com/oauth2/v4/token";
			String urlParameters = "code="+ code + 
					"&client_id=" + clientId +
					"&client_secret=" + clientSecret +
					"&redirect_uri="+ redirectUri + 
					"&grant_type=authorization_code";
			
			byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
			
			HttpURLConnection con = null;
	        try {

	            URL myurl = new URL(url);
	            con = (HttpURLConnection) myurl.openConnection();

	            con.setDoOutput(true);
	            con.setRequestMethod("POST");
	            con.setRequestProperty("User-Agent", "Java client");
	            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

	            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
	                wr.write(postData);
	            }

	            StringBuilder content;

	            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
	                String line;
	                content = new StringBuilder();
	                while ((line = in.readLine()) != null) {
	                    content.append(line);
	                    content.append(System.lineSeparator());
	                }
	            }

	            logger.info("Google returned " + content.toString());
	            Gson gson = new GsonBuilder().setPrettyPrinting().create();
	            GoogleTokenResponse tokenResponse = gson.fromJson(content.toString(), GoogleTokenResponse.class);

	            GoogleUserInfo userInfo = retrieveProfile(tokenResponse.getAccessToken());
	            
	            String domains = props.getProperty("google.domains");
	            if (StringUtils.isNotBlank(domains) && !"*".equals(domains)) {
	            	HashSet<String> domainSet = new HashSet<>();
	            	for (String domain :  domains.split(",")) {
	            		domainSet.add(domain.trim());
	            	}
	            	
	            	boolean found = false;
	            	String email = userInfo.getEmail();
	            	if (email != null) {
	            		int k = email.indexOf('@');
	            		if (k > 0) {
	            			if (domainSet.contains(email.substring(k + 1))) {
	            				found = true;
	            			}
	            		}
	            	}
	            	
	            	if (!found) {
	            		return new RedirectResult("~");
	            	}
	            }
	            
	            AuthenticatedUser user = new AuthenticatedUser(userInfo.getEmail(), userInfo.getName());
	            user.getFields().put("token", tokenResponse.getAccessToken());
	            user.getFields().put("refreshToken", tokenResponse.getRefreshToken());
	            user.getFields().put("tokenType", tokenResponse.getTokenType());
	            user.getFields().put("tokenExpireAt", System.currentTimeMillis() + tokenResponse.getExpiresIn() * 1000);
	    		user.getFields().put("google_id", userInfo.getId());
	    		user.getFields().put("firstName", userInfo.getGivenName());
	    		user.getFields().put("lastName", userInfo.getFamilyName());
	    		user.getFields().put("gender", userInfo.getGender());
	    		user.getFields().put("locale", userInfo.getLocale());
	    		user.getFields().put("avatar", userInfo.getPicture());
	    		user.getFields().put("email", userInfo.getEmail());
	    		
	    		SkySSOService service = controller.getService(SkySSOService.class);
	    		
	    		user.setDomain(service.getDomain(request));
	    		user.setAuthBackend("GOOGLE");
	    		user.setAuthDate(new Date());
	    		
	    		OceanUser oceanUser = service.validateUser(user);
	    		
	    		if (!sm.isLogged() && oceanUser != null) {
	    			sm.getUserSession().login(oceanUser);
	    		}
	    		
	    		String state = request.getParameter("state");
	    		
	    		logger.info("User " + userInfo.getEmail() + " logged in with token Bearer " + tokenResponse.getAccessToken() + "; state = " + state + "; refreshToken " + tokenResponse.getRefreshToken());
	    		
	    		return new RedirectResult(state == null ? "~": state);
	    		
	        } finally {
	        	if (con != null) {
	        		con.disconnect();
	        	}
	        }
	        
		} catch (Exception e) {
        	e.printStackTrace();
		}
		
		
		return null;
	}
	
	private GoogleUserInfo retrieveProfile(String token) throws IOException {
		HttpURLConnection con = null;
        try {

            URL myurl = new URL("https://www.googleapis.com/oauth2/v1/userinfo?alt=json");
            con = (HttpURLConnection) myurl.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", "Bearer " + token);
            
            StringBuilder content;

            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String line;
                content = new StringBuilder();
                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            GoogleUserInfo userInfo = gson.fromJson(content.toString(), GoogleUserInfo.class);
            return userInfo;
        } finally {
        	if (con != null) {
        		con.disconnect();
        	}
        }
	}
}
