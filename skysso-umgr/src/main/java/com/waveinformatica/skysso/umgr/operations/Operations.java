/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.operations;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.input.PasswordFieldHandler;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.umgr.dao.UserDAO;
import com.waveinformatica.skysso.umgr.entities.Role;
import com.waveinformatica.skysso.umgr.entities.User;
import com.waveinformatica.skysso.web.annotations.RequiresAuthorization;

@OperationProvider(namespace = "admin/sso/umgr")
@RequiresAuthorization({"sysadmin","admin"})
public class Operations {
	
	private static final Logger logger = LoggerUtils.getLogger(Operations.class);
    
    @Inject
    private ObjectFactory factory;
    
    @Operation("users/list")
    @RootOperation(title = "Utenti", iconUrl = "user", section = "User Management")
    public TableResult listUsers(OperationScope opScope) {
    		TableResult result = factory.newInstance(TableResult.class);
    		result.putExtra("page-title", "Utenti");
    		
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		List<User> users = userDAO.listUsers(result);
    		logger.fine("Got " + (users == null ? "a null list" : users.size() + " users"));
    		result.setTableData(users, User.class);
    		result.addRowOperation("admin/sso/umgr/users/edit", "Modifica");
    		result.addRowOperation("admin/sso/umgr/users/roles/list", "Ruoli");
    		result.addRowOperation("admin/sso/umgr/users/setpassword", "Set Password");
    		result.addRowOperation("admin/sso/umgr/users/setalias", "Assegna Alias");
    		result.addTableOperation("admin/sso/umgr/users/edit", "Aggiungi");
    		result.setMaxOperations(3);
    		
    		opScope.addBreadcrumbLink("admin/", "administration");
    		opScope.addBreadcrumbLink("admin/sso/umgr/users/list", "Utenti");
    		
        return result;
    }
    
    @Operation("users/edit")
    public InputResult editUser(@Param("username") String username, OperationScope opScope) throws UnsupportedEncodingException {
    		InputResult result = factory.newInstance(InputResult.class);
		
    		opScope.addBreadcrumbLink("admin/", "administration");
    		opScope.addBreadcrumbLink("admin/sso/umgr/users/list", "Utenti");
    		
		final UserDAO userDAO = factory.newInstance(UserDAO.class);
		User user = userDAO.findUser(username);
		if (user != null) {
			result.putExtra("page-title", "Modifica utente");
			result.setAction("admin/sso/umgr/users/save");
			result.setData(user);
			opScope.addBreadcrumbLink("admin/sso/umgr/users/edit?username" + URLEncoder.encode(username, "UTF-8"), "Modifica");
		} else {
			result.putExtra("page-title", "Nuovo utente");
			result.setAction("admin/sso/umgr/users/create");
			opScope.addBreadcrumbLink("admin/sso/umgr/users/edit", "Modifica");
		}
		
		return result;
    }
    
    @Operation("users/save")
    public MessageResult saveUser(@Param("username") @InputResult.Field(readOnly=true) String username, @Param("fullname") String fullname, @Param("imageUri") String imageUri, @Param("email") String email) {
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		
    		User user = new User();
    		user.setUsername(username);
    		user.setFullname(fullname);
    		user.setImageUri(imageUri);
    		user.setEmail(email);
    		
    		MessageResult result = factory.newInstance(MessageResult.class);
    		result.setConfirmAction("admin/sso/umgr/users/list");
    		if (userDAO.updateUser(user)) {
    			result.setType(MessageResult.MessageType.CONFIRM);
    			result.setMessage("L'utente " + username + " è stato modificato con successo");
    		} else {
    			result.setType(MessageResult.MessageType.WARNING);
    			result.setMessage("Si è verificato un errore durante l'aggiornamento dell'utente " + username);
    		}
    		
    		return result;
    }
    
    @Operation("users/create")
    public MessageResult createUser(@Param("username") @InputResult.Field(required=true) String username, @Param("fullname") String fullname, @Param("imageUri") String imageUri, @Param("email") String email) {
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		
    		User user = new User();
    		user.setUsername(username.toLowerCase());
    		user.setFullname(fullname);
    		user.setImageUri(imageUri);
    		user.setEmail(email);
    		
    		MessageResult result = factory.newInstance(MessageResult.class);
    		if (userDAO.createUser(user)) {
    			result.setConfirmAction("admin/sso/umgr/users/list");
    			result.setType(MessageResult.MessageType.CONFIRM);
    			result.setMessage("L'utente " + username + " è stato creato con successo");
    		} else {
    			result.setConfirmAction("admin/sso/umgr/users/edit");
    			result.setType(MessageResult.MessageType.WARNING);
    			result.setMessage("L'utente " + username + " è già esistente");
    		}
    		
    		return result;
    }
    
    @Operation("users/setpassword")
    public InputResult setUserPassword(@Param("username") String username, OperationScope opScope) throws UnsupportedEncodingException {
    		InputResult result = factory.newInstance(InputResult.class);
    		result.putExtra("page-title", "Imposta password");
    		
    		opScope.addBreadcrumbLink("admin/", "administration");
		opScope.addBreadcrumbLink("admin/sso/umgr/users/list", "Utenti");
		opScope.addBreadcrumbLink("admin/sso/umgr/users/setpassword?username=" + URLEncoder.encode(username, "UTF-8"), "Set Password");
		
		result.setAction("admin/sso/umgr/users/savepassword");
		result.setValue("username", username);
		
    		return result;
    }
    
    @Operation("users/savepassword")
    public MessageResult saveUserPassword(@Param("username") @InputResult.Field(readOnly=true) String username, @Param("password") @InputResult.Field(handler = PasswordFieldHandler.class) String password) throws UnsupportedEncodingException {
    		
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		MessageResult result = factory.newInstance(MessageResult.class);
    		
    		if (userDAO.setPassword(username, password)) {
    			result.setConfirmAction("admin/sso/umgr/users/list");
    			result.setType(MessageResult.MessageType.CONFIRM);
    			result.setMessage("Password impostata con successo");
    		} else {
    			result.setConfirmAction("admin/sso/umgr/users/setpassword?username=" + URLEncoder.encode(username, "UTF-8"));
    			result.setType(MessageResult.MessageType.CRITICAL);
    			result.setMessage("Si è verificato un problema inatteso impostando la password");
    		}
    		
    		return result;
    }
    
    @Operation("roles/list")
    @RootOperation(title = "Ruoli", iconUrl = "users", section = "User Management")
    public TableResult listRoles(OperationScope opScope) {
    		TableResult result = factory.newInstance(TableResult.class);
    		result.putExtra("page-title", "Ruoli");
    		
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		List<Role> roles = userDAO.listRoles(result);
    		result.setTableData(roles, Role.class);
    		result.addRowOperation("admin/sso/umgr/roles/edit", "Modifica");
    		result.addTableOperation("admin/sso/umgr/roles/edit", "Aggiungi");
    		result.setMaxOperations(3);
    		
    		opScope.addBreadcrumbLink("admin/", "administration");
    		opScope.addBreadcrumbLink("admin/sso/umgr/roles/list", "Ruoli");
    		
        return result;
    }
    
    @Operation("roles/edit")
    public InputResult editRole(@Param("id") String id, OperationScope opScope) throws UnsupportedEncodingException {
    		InputResult result = factory.newInstance(InputResult.class);
		
    		opScope.addBreadcrumbLink("admin/", "administration");
    		opScope.addBreadcrumbLink("admin/sso/umgr/roles/list", "Ruoli");
    		
		final UserDAO userDAO = factory.newInstance(UserDAO.class);
		Role role = userDAO.findRole(id);
		if (role != null) {
			result.putExtra("page-title", "Modifica ruolo");
			result.setAction("admin/sso/umgr/roles/save");
			result.setData(role);
			opScope.addBreadcrumbLink("admin/sso/umgr/roles/edit?id" + URLEncoder.encode(id, "UTF-8"), "Modifica");
		} else {
			result.putExtra("page-title", "Nuovo ruolo");
			result.setAction("admin/sso/umgr/roles/create");
			opScope.addBreadcrumbLink("admin/sso/umgr/roles/edit", "Modifica");
		}
		
		return result;
    }
    
    @Operation("roles/save")
    public MessageResult saveRole(@Param("id") @InputResult.Field(readOnly=true) String id, @Param("name") String name) {
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		
    		Role role = new Role();
    		role.setId(id);
    		role.setName(name);
    		
    		MessageResult result = factory.newInstance(MessageResult.class);
    		result.setConfirmAction("admin/sso/umgr/roles/list");
    		if (userDAO.updateRole(role)) {
    			result.setType(MessageResult.MessageType.CONFIRM);
    			result.setMessage("Ruolo " + id + " modificato con successo");
    		} else {
    			result.setType(MessageResult.MessageType.WARNING);
    			result.setMessage("Si è verificato un errore durante l'aggiornamento del ruolo " + id);
    		}
    		
    		return result;
    }
    
    @Operation("roles/create")
    public MessageResult createRole(@Param("id") @InputResult.Field(required=true) String id, @Param("name") String name) {
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		
    		Role role = new Role();
    		role.setId(id);
    		role.setName(name);
    		
    		MessageResult result = factory.newInstance(MessageResult.class);
    		if (userDAO.createRole(role)) {
    			result.setConfirmAction("admin/sso/umgr/roles/list");
    			result.setType(MessageResult.MessageType.CONFIRM);
    			result.setMessage("Ruolo " + id + " creato con successo");
    		} else {
    			result.setConfirmAction("admin/sso/umgr/roles/edit");
    			result.setType(MessageResult.MessageType.WARNING);
    			result.setMessage("Ruolo " + id + " già esistente");
    		}
    		
    		return result;
    }
    
    @Operation("users/roles/list")
    public TableResult listUserRoles(@Param("username") String username, OperationScope opScope) throws UnsupportedEncodingException {
    		TableResult result = factory.newInstance(TableResult.class);
    		result.putExtra("page-title", "Ruoli dell'utente " + username);
    		
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		User user = userDAO.findUser(username);
    		
    		result.setTableData(user.getRoles(), Role.class);
    		result.addRowOperation("admin/sso/umgr/users/roles/delete?username="+ URLEncoder.encode(username, "UTF-8"), "Revoca");
    		result.addTableOperation("admin/sso/umgr/users/roles/add?username=" + URLEncoder.encode(username, "UTF-8"), "Assegna");
    		result.setMaxOperations(3);
    		
    		opScope.addBreadcrumbLink("admin/", "administration");
    		opScope.addBreadcrumbLink("admin/sso/umgr/users/list", "Utenti");
    		opScope.addBreadcrumbLink("admin/sso/umgr/users/roles/list?username=" + URLEncoder.encode(username, "UTF-8"), "Ruoli");
    		
        return result;
    }
    
    @Operation("users/roles/add")
    public InputResult addUserRole(@Param("username") String username, OperationScope opScope) throws UnsupportedEncodingException {
    		InputResult result = factory.newInstance(InputResult.class);
    		result.putExtra("page-title", "Assegna ruolo");
		
    		opScope.addBreadcrumbLink("admin/", "administration");
    		opScope.addBreadcrumbLink("admin/sso/umgr/users/list", "Utenti");
    		opScope.addBreadcrumbLink("admin/sso/umgr/users/roles/list?username=" + URLEncoder.encode(username, "UTF-8"), "Ruoli");
    		opScope.addBreadcrumbLink("admin/sso/umgr/users/roles/add?username=" + URLEncoder.encode(username, "UTF-8"), "Assegna");
    		
    		result.getExtras().put("username", username);
    		result.setAction("admin/sso/umgr/users/roles/save");
    		result.getField("username").setValue(username);
    		result.getField("id").setLabel("Ruolo");
    				
		return result;
    }
    
    @Operation("users/roles/save")
    public MessageResult saveUserRole(@Param("username") @InputResult.Field(readOnly=true) String username, @Param("id") @InputResult.Field(handler = RoleSelectFieldHandler.class) String id) throws UnsupportedEncodingException {
    		
    		MessageResult result = factory.newInstance(MessageResult.class);
    		final UserDAO userDAO = factory.newInstance(UserDAO.class);
    		
		if (userDAO.addUserRole(username, id)) {
			result.setConfirmAction("admin/sso/umgr/users/roles/list?username=" + URLEncoder.encode(username, "UTF-8"));
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setMessage("Ruolo " + id + " assegnato con successo");
		} else {
			result.setConfirmAction("admin/sso/umgr/users/roles/add?username=" + URLEncoder.encode(username, "UTF-8"));
			result.setType(MessageResult.MessageType.WARNING);
			result.setMessage("Si è verificato un problema assegnado il ruolo " + id + " all'utente " + username);
		}
   		
    		return result;
    }
    
    @Operation("users/roles/delete")
    public MessageResult deleteUserRole(@Param("username") String username, @Param("id") String id) throws UnsupportedEncodingException {
    		
    		MessageResult result = factory.newInstance(MessageResult.class);
		final UserDAO userDAO = factory.newInstance(UserDAO.class);
		
		if (userDAO.removeUserRole(username, id)) {
			result.setConfirmAction("admin/sso/umgr/users/roles/list?username=" + URLEncoder.encode(username, "UTF-8"));
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setMessage("Ruolo " + id + " revocato con successo");
		} else {
			result.setConfirmAction("admin/sso/umgr/users/roles/list?username=" + URLEncoder.encode(username, "UTF-8"));
			result.setType(MessageResult.MessageType.WARNING);
			result.setMessage("Si è verificato un problema revocando il ruolo " + id + " all'utente " + username);
		}
    		return result;
    }
    
    @Operation("users/setalias")
    public InputResult setUserAlias(@Param("username") String username, OperationScope opScope) throws UnsupportedEncodingException {
    		InputResult result = factory.newInstance(InputResult.class);
		result.putExtra("page-title", "Assegna alias");
		
		opScope.addBreadcrumbLink("admin/", "administration");
		opScope.addBreadcrumbLink("admin/sso/umgr/users/list", "Utenti");
		opScope.addBreadcrumbLink("admin/sso/umgr/users/setalias?username=" + URLEncoder.encode(username, "UTF-8"), "Assegna Alias");
		
		result.getExtras().put("username", username);
		result.setAction("admin/sso/umgr/users/savealias");
		result.setValue("username", username);
		
		return result;
    }
  
    @Operation("users/savealias")
    public MessageResult saveUserAlias(@Param("username") @InputResult.Field(readOnly=true) String username, @Param("alias") @InputResult.Field(handler = UsernameSelectFieldHandler.class) String aliasId) throws UnsupportedEncodingException {
    	
    		MessageResult result = factory.newInstance(MessageResult.class);
		final UserDAO userDAO = factory.newInstance(UserDAO.class);
		if (userDAO.setAlias(username, StringUtils.isBlank(aliasId) ? null : aliasId)) {
			result.setConfirmAction("admin/sso/umgr/users/list");
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setMessage("Alias assegnato con successo");
		} else {
			result.setConfirmAction("admin/sso/umgr/users/setalias?username=" + URLEncoder.encode(username, "UTF-8"));
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setMessage("Si è verificato un problema inatteso assegnando l'alias");
		}
		
		return result;
    }
}
