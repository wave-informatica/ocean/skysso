/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.sso;

import java.util.logging.Logger;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.umgr.dao.UserDAO;
import com.waveinformatica.skysso.umgr.entities.User;
import com.waveinformatica.skysso.web.annotations.UsersManager;
import com.waveinformatica.skysso.web.base.IUsersManager;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;

@UsersManager
public class UserManager implements IUsersManager {
	
	private static final Logger logger = LoggerUtils.getLogger(UserManager.class);

	@Inject
	private ObjectFactory factory;
	
	@Override
	public OceanUser validateUser(AuthenticatedUser authUser) {
		final UserDAO userDAO = factory.newInstance(UserDAO.class);
		User user = userDAO.findUser(authUser.getUserId());
		if (user == null) {
			logger.warning("No user found for authenticated user " + authUser.getUserId() + " service (" + authUser.getAuthBackend() + ")");
			user = new User();
			user.setUsername(authUser.getUserId());
			user.setFullname(authUser.getFullName());
			
			Object emailObj = authUser.getFields().get("email");
			if (emailObj != null) {
				user.setEmail(emailObj.toString());
			}
			
			Object imageUrlObj = authUser.getFields().get("imageUrl");
			if (imageUrlObj == null) {
				imageUrlObj = authUser.getFields().get("avatar");
			}
			if (imageUrlObj != null) {
				user.setImageUri(imageUrlObj.toString());
			}
			
			if (!userDAO.createUser(user)) {
				user = null;
			}
			
			logger.info("User " + authUser.getUserId() + " auto registered");
		} else {
			User alias = user.getAlias();
			if (alias != null) {
				logger.info("Using alias " + alias.getUsername() + " for user " + user.getUsername());
				user = alias;
			}
		}
		
		if (user != null) {
			for (String p : authUser.getFields().keySet()) {
				user.getFields().put(p, authUser.getFields().get(p));
			}
		}
		return user;
	}

	@Override
	public String getForgotPasswordOperation() {
		return null;
	}

	@Override
	public String getRegisterOperation() {
		return null;
	}

}
