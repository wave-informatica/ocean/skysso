/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.filtering.FilterBuilder;
import com.waveinformatica.ocean.core.filtering.FilterProvider;
import com.waveinformatica.ocean.core.filtering.QueryFilter;
import com.waveinformatica.skysso.umgr.entities.Role;
import com.waveinformatica.skysso.umgr.entities.User;
import com.waveinformatica.skysso.umgr.entities.UserCredential;

public class UserDAO {
	
	//private static final Logger logger = LoggerUtils.getLogger(UserDAO.class);

	@Inject
    private ObjectFactory factory;
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	public List<User> listUsers(FilterProvider filter) {
		FilterBuilder fb = factory.newInstance(FilterBuilder.class);
		fb.init(User.class);
		QueryFilter qf = fb.getJpqlFilter(filter, "x");
		
		TypedQuery<User> query = em.createQuery("select x from User x" + (StringUtils.isNotBlank(qf.getCondition()) ? " where " + qf.getCondition() : "") + " order by x.username desc", User.class);
		qf.fillQueryParams(query);
		
		return query.getResultList();
	}
	
	public User findUser(String username) {
		return username == null ? null :  em.find(User.class, username);
	}
	
	public boolean createUser(User user) {
		em.getTransaction().begin();
		User u = findUser(user.getUsername());
		if (u == null) {
			u = new User();
			u.setUsername(user.getUsername());
			u.setFullname(user.getFullname());
			u.setImageUri(user.getImageUri());
			u.setEmail(user.getEmail());
			
			em.persist(u);
			em.getTransaction().commit();
			return true;
		} else {
			em.getTransaction().rollback();
			return false;
		}
	}
	
	public boolean updateUser(User user) {
		em.getTransaction().begin();
		User u = findUser(user.getUsername());
		if (u != null) {
			u.setFullname(user.getFullname());
			u.setImageUri(user.getImageUri());
			u.setEmail(user.getEmail());
			
			em.getTransaction().commit();
			return true;
		} else {
			em.getTransaction().rollback();
			return false;
		}
	}
	
	public boolean setPassword(String username, String password) {
		em.getTransaction().begin();
		User u = findUser(username);
		if (u != null) {
			UserCredential credential = u.getCredential();
			if (credential == null) {
				credential = new UserCredential();
				credential.setUsername(username);
				credential.encryptAndSetPassword(password);
				em.persist(credential);
				u.setCredential(credential);
				em.merge(u);
			} else {
				credential.encryptAndSetPassword(password);
			}
			em.getTransaction().commit();
			return true;
		} else {
			em.getTransaction().rollback();
			return false;
		}
	}
	
	public boolean setAlias(String username, String aliasId) {
		em.getTransaction().begin();
		User u = findUser(username);
		if (u != null) {
			if (aliasId == null || aliasId.equals(username)) {
				u.setAlias(null);
				em.getTransaction().commit();
				return true;
			}
			
			User a = findUser(aliasId);
			if (a != null) {
				u.setAlias(a);
				em.getTransaction().commit();
				return true;
			}
		}
		
		em.getTransaction().rollback();
		return false;
	}
	
	public List<Role> listRoles(FilterProvider filter) {
		String condition = "";
		QueryFilter qf = null;
		
		if (filter != null) {
			FilterBuilder fb = factory.newInstance(FilterBuilder.class);
			fb.init(Role.class);
			qf = fb.getJpqlFilter(filter, "x");
			condition = qf.getCondition();
		}
		
		
		TypedQuery<Role> query = em.createQuery("select x from Role x" + (StringUtils.isNotBlank(condition) ? " where " + condition : "") + " order by x.id desc", Role.class);
		if (qf != null) {
			qf.fillQueryParams(query);
		}
		
		return query.getResultList();
	}
	
	public Role findRole(String id) {
		return id == null ? null :  em.find(Role.class, id.toLowerCase());
	}
	
	public boolean createRole(Role role) {
		em.getTransaction().begin();
		Role r = findRole(role.getId());
		if (r == null) {
			r = new Role();
			r.setId(role.getId());
			r.setName(role.getName());
			
			em.persist(r);
			em.getTransaction().commit();
			return true;
		} else {
			em.getTransaction().rollback();
			return false;
		}
	}
	
	public boolean updateRole(Role role) {
		em.getTransaction().begin();
		Role r = findRole(role.getId());
		if (r != null) {
			r.setName(role.getName());
			
			em.getTransaction().commit();
			return true;
		} else {
			em.getTransaction().rollback();
			return false;
		}
	}
	
	public boolean addUserRole(String username, String id) {
		try {
			em.getTransaction().begin();
			
			User user = findUser(username);
			Role role = findRole(id);
			if (user != null && role != null) {
				user.getRoles().add(role);
				em.merge(user);
				em.getTransaction().commit();
				return true;
			} else {
				em.getTransaction().rollback();
				return false;
			}
		} catch (RollbackException e) {
			return false;
		}
	}
	
	public boolean removeUserRole(String username, String id) {
		try {
			em.getTransaction().begin();
			User user = findUser(username);
			Role role = findRole(id);
			if (user != null && role != null) {
				boolean removed = user.getRoles().remove(role);
				em.merge(user);
				em.getTransaction().commit();
				return removed;
			} else {
				em.getTransaction().rollback();
				return false;
			}
		} catch (RollbackException e) {
			return false;
		}
	}
	
	public User defaultAdminUser() {
		try {
			final String defaultRoleId = "sysadmin";
			final String defaultUsername = "admin";
			em.getTransaction().begin();
			Role sysadmin = em.find(Role.class, defaultRoleId);
			if (sysadmin == null) {
				sysadmin = new Role();
				sysadmin.setId(defaultRoleId);
				sysadmin.setName("System Administrator");
				em.persist(sysadmin);
			}
			
			User user = new User();
			user.setUsername(defaultUsername);
			user.setFullname("Administrator");
			em.persist(user);
			
			UserCredential credential = new UserCredential();
			credential.setUsername(defaultUsername);
			credential.encryptAndSetPassword(defaultUsername);
			em.persist(credential);
			
			user.setCredential(credential);
			user.getRoles().add(sysadmin);
			em.merge(user);
			
			em.getTransaction().commit();
			return user;
		} catch (RollbackException e) {
			return null;
		}
	}
}
