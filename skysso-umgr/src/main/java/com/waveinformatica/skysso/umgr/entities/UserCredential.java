/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.entities;

import java.security.MessageDigest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.codec.binary.Base64;

/**
 * @author Luca Lattore
 */
@Entity
@Table(name = "skysso_user_credentials")
public class UserCredential {

	@Id
	@Column
	private String username;

	@Column(name = "enc_pw")
	private String encryptedPassword;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void encryptAndSetPassword(String password) {
		this.encryptedPassword = digestB64(password);
	}
	
	public boolean matchPassword(String password) {
		if (password != null && encryptedPassword != null) {
			return encryptedPassword.equals(password) || encryptedPassword.equals(digestB64(password));
		}
		
		return false;
	}
	
	private static String digestB64(String s) {
		String result = null;
		
		if (s != null) {
			try {
				result = Base64.encodeBase64String(MessageDigest.getInstance("MD5").digest(s.getBytes("UTF-8")));
			} catch (Exception e) {
				// ignore
			}
		}
		
		return result;
	}
	
}
