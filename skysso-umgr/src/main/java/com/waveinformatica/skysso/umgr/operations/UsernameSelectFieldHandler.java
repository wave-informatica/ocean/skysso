/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.operations;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FormField;
import com.waveinformatica.ocean.core.controllers.results.input.ComboFieldHandler;
import com.waveinformatica.skysso.umgr.dao.UserDAO;

public class UsernameSelectFieldHandler extends ComboFieldHandler {

	@Inject
    private ObjectFactory factory;
	
	@Override
	public void init(InputResult result, OperationInfo opInfo, ParameterInfo paramInfo, FormField field) {
		final UserDAO userDAO = factory.newInstance(UserDAO.class);
		
		String username = result.getExtra("username").toString();
		userDAO.listUsers(null).forEach((user) -> {
			if (!user.getUsername().equals(username)) {
				addOption(user.getUsername(), user.getUsername() + (user.getFullname() == null ? "" : (" (" + user.getFullname() + ")")));
			}
		});
		
	}

	
}
