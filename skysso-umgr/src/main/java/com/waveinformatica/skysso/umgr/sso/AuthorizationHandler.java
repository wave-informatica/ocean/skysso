/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.sso;

import java.util.logging.Logger;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.AuthorizeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.operations.Administration;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.umgr.entities.User;
import com.waveinformatica.skysso.web.annotations.RequiresAuthorization;
import com.waveinformatica.skysso.web.annotations.RequiresPermission;

@CoreEventListener(eventNames = CoreEventName.AUTHORIZE_OPERATION)
public class AuthorizationHandler implements IEventListener {

	private static final Logger logger = LoggerUtils.getLogger(AuthorizationHandler.class);
	
	@Inject
	private UserSession userSession;
	
	@Override
	public void performAction(Event event) {
		AuthorizeOperationEvent evt = (AuthorizeOperationEvent) event;
		User user = (User)userSession.getPrincipal();
		if (user != null) {
			OperationInfo operationInfo = evt.getOperationInfo();
			Class<?> operationClass = operationInfo.getCls();
			
			if (operationClass.equals(Administration.class) && !user.hasRole("sysadmin")) {
				evt.setAuthorized(false);
				return;
			}
			
			RequiresAuthorization an = operationInfo.getExtraAnnotation(RequiresAuthorization.class);
			if (an == null) {
				an = (RequiresAuthorization)operationClass.getAnnotation(RequiresAuthorization.class);
			}
			
			if (an != null) {
				for (String roleId : an.value()) {
					if (user.hasRole(roleId)) {
						logger.finest("User " + user.getUsername() +" authorized for " + operationInfo.getFullName() + " by role " + roleId);
						evt.setAuthorized(true);
						return;
					}
				}
				
				evt.setAuthorized(false);
			} else {
				RequiresPermission p = operationInfo.getExtraAnnotation(RequiresPermission.class);
				if (p != null) {
					if (user.hasPermissions(p.value())) {
						logger.info("User " + user.getUsername() + " authorized by permissions " + p.value());
						evt.setAuthorized(true);
					} else {
						logger.warning("Access denied: user " + user.getUsername() + " has no permissions " + p.value());
						evt.setAuthorized(false);
					}
					
					return;
				}
				
				evt.setAuthorized(true);
			}
		}
		
	}

}
