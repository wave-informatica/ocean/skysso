/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.waveinformatica.ocean.core.controllers.results.TableResult.TableResultColumn;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.skysso.umgr.serializers.UsernameSerializer;
import com.waveinformatica.skysso.web.dto.Permissions;

/**
 * @author Luca Lattore
 */
@Entity
@Table(name = "skysso_users")
public class User implements OceanUser, Cloneable {

	private static final long serialVersionUID = -71975012329964963L;

	@Id
	@Column
	private String username;

	@Column
	private String fullname;
	
	@Column(name = "image_uri", length = 4000)
	private String imageUri;
	
	@Column
	private String email;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "skysso_user_roles", joinColumns = @JoinColumn(name = "username"), inverseJoinColumns = @JoinColumn(name = "role_id"), 
	uniqueConstraints=@UniqueConstraint(columnNames={"username","role_id"}))
	private List<Role> roles = new ArrayList<Role>();
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="username")
	private UserCredential credential;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="alias")
	@TableResultColumn(columnSerializer = UsernameSerializer.class)
	private User alias;
	
	@Transient
	private final Map<String, Object> fields = new HashMap<String, Object>();
	
	@Override
	public User clone() throws CloneNotSupportedException
	{
		return (User)super.clone();
	}
	
	@Override
	public Long getId() {
		return username == null ? -1L : (long)username.hashCode();
	}
	
	@Override
	public String toString()
	{
		return fullname == null ? username : fullname;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public UserCredential getCredential() {
		return credential;
	}
	
	public void setCredential(UserCredential credential) {
		this.credential = credential;
	}
	
	public User getAlias() {
		return alias;
	}
	
	public void setAlias(User alias) {
		this.alias = alias;
	}
	
	public boolean hasRole(String roleId) {
		if (roleId != null && roles != null) {
			for (Role role : roles) {
				if (roleId.equalsIgnoreCase(role.getId())) {
					return true;
				}
			}
		}
		
		return false;
	}

	public boolean hasPermissions(Permissions permissions) {
		return permissions == null ? false : hasRole(permissions.toString());
	}
	
	public Map<String, Object> getFields() {
		return fields;
	}
}
