/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.sso;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.skysso.umgr.dao.UserDAO;
import com.waveinformatica.skysso.umgr.entities.User;
import com.waveinformatica.skysso.umgr.entities.UserCredential;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthResultCode;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;

/**
*
* @author Luca Lattore
*/
@BackendAuthService("UMGR")
@OperationProvider(namespace = "sso/umgr")
public class UserLoginService implements IBackendAuthService {

	//private static final Logger logger = LoggerUtils.getLogger(UserLoginService.class);
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		return true;
	}

	@Override
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {
		if (StringUtils.isBlank(request.getUserName())) {
			return new LoginResponse(AuthResultCode.USER_NOT_FOUND);
		} else if (StringUtils.isBlank(request.getPassword())) {
			return new LoginResponse(AuthResultCode.WRONG_PASSWORD);
		}
		
		final UserDAO userDAO = factory.newInstance(UserDAO.class);
		
		User user = userDAO.findUser(request.getUserName().toLowerCase());
		if (user == null) {
			if ("admin".equals(request.getUserName()) && "admin".equals(request.getPassword())) {
				user = userDAO.defaultAdminUser();
				if (user == null) {
					return new LoginResponse(AuthResultCode.USER_NOT_FOUND);
				}
			} else {
				return new LoginResponse(AuthResultCode.USER_NOT_FOUND);
			}
		}
		
		UserCredential credential = user.getCredential();
		if (credential == null || !credential.matchPassword(request.getPassword())) {
			return new LoginResponse(AuthResultCode.WRONG_PASSWORD);
		}
		
		AuthenticatedUser authUser = new AuthenticatedUser(user.getUsername(), user.getFullname());
		authUser.setAuthBackend("UMGR");
		authUser.setAuthDate(new Date());
		
		return new LoginResponse(authUser);
	}

	@Override
	public UserNameType getUserNameType() {
		return UserNameType.TEXT;
	}

	@Override
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
		return null;
	}
	
}
