/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.umgr.serializers;

import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult.CellSerializer;
import com.waveinformatica.ocean.core.controllers.results.TableResult.TableCell;
import com.waveinformatica.ocean.core.controllers.results.TableResult.TableColumn;
import com.waveinformatica.ocean.core.controllers.results.TableResult.TableRow;
import com.waveinformatica.skysso.umgr.entities.User;

public class UsernameSerializer implements CellSerializer {

	@Override
	public void serialize(TableResult result, TableRow row, TableColumn column, TableCell cell) {
		User user = (User)cell.getOriginalValue();
		cell.setValue(user == null ? "" : user.getUsername());
	}

}
