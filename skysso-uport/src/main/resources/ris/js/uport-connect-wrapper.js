const Connect = window.uportconnect.Connect
const SimpleSigner = window.uportconnect.SimpleSigner

// uPort connect
const uportConnect = function(appName, clientId, secret, loginUrl) {
	const connect = new Connect(appName, {
		network: 'rinkeby',
		clientId: clientId,
		signer: SimpleSigner(secret)
	})
	connect.requestCredentials({
			requested: ['name', 'avatar', 'email']
		})
		.then((credentials) => {
			// Do login
			console.log(credentials)
			document.location.href = loginUrl + '?' + $.param(credentials)
				//document.location.href = loginUrl + '?address=' + credentials.address + '&email=' + credentials.email + '&name=' + encodeURIComponent(credentials.name)
		})
}
