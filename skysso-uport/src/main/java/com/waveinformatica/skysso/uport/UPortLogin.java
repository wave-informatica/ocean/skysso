/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.uport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;

/**
*
* @author Luca Lattore
*/
@BackendAuthService("UPORT")
@OperationProvider(namespace = "sso/uport")
public class UPortLogin implements IBackendAuthService {
	
	private static final Logger logger = LoggerUtils.getLogger(UPortLogin.class);
	
	@Inject
	private Configuration config;
	
	@Inject
	private CoreController controller;
	
	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager sm;

	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		PersistedProperties props = config.getCustomProperties("skysso.uport");
		return StringUtils.isNotBlank(props.getProperty("uport.appname")) && 
				StringUtils.isNotBlank(props.getProperty("uport.clientid")) && 
				StringUtils.isNotBlank(props.getProperty("uport.secret"));
	}

	@Override
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {
		return null;
	}

	@Override
	public UserNameType getUserNameType() {
		return null;
	}

	@Override
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
		HttpServletRequest request = Context.get().getRequest();
		String loginUrl = config.getSiteProperty("site.url", "http://localhost:8080");
		if (loginUrl.endsWith("/")) {
			loginUrl = loginUrl.substring(0, loginUrl.length() - 1);
		}
		if (!loginUrl.endsWith(request.getContextPath())) {
			loginUrl = loginUrl + request.getContextPath();
		}
		loginUrl = loginUrl + "/sso/uport/login";
		
		PersistedProperties props = config.getCustomProperties("skysso.uport");
		String script = "uportConnect('"+ props.getProperty("uport.appname") + "','" + 
				props.getProperty("uport.clientid") + "','" + 
				props.getProperty("uport.secret") + "', '" + 
				loginUrl + "'); return false";
		
		List<AuthenticationLink> links = new ArrayList<AuthenticationLink>();
		AuthenticationLink link = new AuthenticationLink("#", "btn-twitter", "sign-in", "uPort", script);
		Results.addScriptSource(result, "ris/js/uport-connect-0.7.1.js");
		Results.addScriptSource(result, "ris/js/uport-connect-wrapper.js");
		links.add(link);
		return links;
	}
	
	@Operation("login")
	@SkipAuthorization
	public BaseResult uportLoggedIn(@Param("address") String address, @Param("name") String name, @Param("email") String email, @Param("avatar[uri]") String avatar, HttpServletRequest request) {
		
		logger.info("uPort login with address " + address + " (" + name + "); avatar = " + avatar);
		
		AuthenticatedUser user = new AuthenticatedUser(address, name);
		user.getFields().put("email", email);
		user.getFields().put("avatar", avatar);
		
		SkySSOService service = controller.getService(SkySSOService.class);
		user.setDomain(service.getDomain(request));
		user.setAuthBackend("UPORT");
		user.setAuthDate(new Date());
		
		OceanUser oceanUser = service.validateUser(user);
		
		if (!sm.isLogged() && oceanUser != null) {
			sm.getUserSession().login(oceanUser);
		}
		
		return new RedirectResult("~");
	}

}
