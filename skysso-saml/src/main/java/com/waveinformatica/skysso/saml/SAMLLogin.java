/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.saml;

import com.lastpass.saml.IdPConfig;
import com.lastpass.saml.SAMLClient;
import com.lastpass.saml.SAMLException;
import com.lastpass.saml.SAMLInit;
import com.lastpass.saml.SAMLUtils;
import com.lastpass.saml.SPConfig;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ResourceRetriever;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;
import com.waveinformatica.skysso.web.util.CryptUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ivano
 */
@BackendAuthService(value = "SAML")
public class SAMLLogin implements IBackendAuthService {
	
	@Inject
	private Configuration config;
	
	@Inject
	private ResourceRetriever retriever;
	
	private IdPConfig idpConfig;
	private SPConfig spConfig;
	private Protocol protocol;
	
	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		
		Properties props = config.getCustomProperties("skysso.saml");
		
		return StringUtils.isNotBlank(props.getProperty("protocol"))
			  && StringUtils.isNotBlank(props.getProperty("idp.metadata"))
			  && StringUtils.isNotBlank(props.getProperty("sp.metadata"))
			  && StringUtils.isNotBlank(props.getProperty("privateKey"));
		
	}
	
	@Override
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {
		
		return null;
		
	}
	
	@Override
	public UserNameType getUserNameType() {
		return UserNameType.TEXT;
	}
	
	@Override
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
		
		try {
			
			SAMLInit.initialize();
			loadConfig();
			
			if (protocol == Protocol.WSFED) {
				
				StringBuilder builder = new StringBuilder();
				
				HttpServletRequest req = Context.get().getRequest();
				String contextPath = req.getContextPath();
				if (contextPath.startsWith("/")) {
					contextPath = contextPath.substring(1);
				}
				
				String contextParams = "redirect=" +
					  URLEncoder.encode(Context.get().getRequest().getParameter("redirect"), "UTF-8");
				
				String wctx = CryptUtils.encryptBase64(contextParams.getBytes(Charset.forName("UTF-8")));
				
				builder.append(idpConfig.getLoginUrl());
				builder.append("?wa=wsignin1.0&wtrealm=");
				builder.append(spConfig.getEntityId());
				builder.append("&wct=");
				builder.append(JsonUtils.formatDate(new Date()));
				builder.append("&wreply=");
				builder.append(spConfig.getEntityId());
				builder.append(URLEncoder.encode(contextPath, "UTF-8"));
				builder.append("/auth/wsfed/authenticate");
				builder.append("&wctx=");
				builder.append(URLEncoder.encode(wctx, "UTF-8"));
				
				return Arrays.asList(new AuthenticationLink(builder.toString(), "btn-github", "cloud", "Federation"));
				
			}
			else if (protocol == Protocol.SAML) {
				
				SAMLClient client = new SAMLClient(spConfig, idpConfig);
				
				// ...
				// when a login link is clicked, create auth request and
				// redirect to the IdP
				String requestId = SAMLUtils.generateRequestId();
				String authrequest = client.generateAuthnRequest(requestId);
				try {
					String url = client.getIdPConfig().getLoginUrl()
						  + "?SAMLRequest=" + URLEncoder.encode(authrequest, "UTF-8");
					// redirect to url...
					
					return Arrays.asList(new AuthenticationLink(url, "btn-github", "cloud", "Federation"));
					
				} catch (UnsupportedEncodingException ex) {
					Logger.getLogger(SAMLLogin.class.getName()).log(Level.SEVERE, null, ex);
				}
				
			}
			
		} catch (Exception e) {
			LoggerUtils.getLogger(SAMLLogin.class).log(Level.SEVERE, "Unexpected error producing authentication link: " + e.getClass().getName() + ": " + e.getMessage());
		}
		
		return null;
	}
	
	private void loadConfig() throws SAMLException, IOException {
		
		Properties props = config.getCustomProperties("skysso.saml");
		
		protocol = Protocol.valueOf(props.getProperty("protocol"));
		
		File file = new File(props.getProperty("idp.metadata"));
		if (file.exists()) {
			idpConfig = new IdPConfig(file);
		} else {
			ResourceInfo ri = retriever.getResource(props.getProperty("idp.metadata"));
			if (ri != null) {
				InputStream is = ri.getInputStream();
				idpConfig = new IdPConfig(is);
			} else {
				LoggerUtils.getLogger(SAMLLogin.class).log(Level.SEVERE, "SAML: Unable to find IdP configuration: \"" + props.getProperty("idp.metadata") + "\"");
			}
		}
		
		file = new File(props.getProperty("sp.metadata"));
		
		if (protocol == Protocol.SAML) {
			
			if (file.exists()) {
				spConfig = new SPConfig(file);
			} else {
				ResourceInfo ri = retriever.getResource(props.getProperty("sp.metadata"));
				if (ri != null) {
					InputStream is = ri.getInputStream();
					spConfig = new SPConfig(is);
				} else {
					LoggerUtils.getLogger(SAMLLogin.class).log(Level.SEVERE, "SAML: Unable to find SP configuration: \"" + props.getProperty("sp.metadata") + "\"");
				}
			}
			
		}
		else if (protocol == Protocol.WSFED) {
			
			if (file.exists()) {
				spConfig = new WsFedSPConfig(file);
			} else {
				ResourceInfo ri = retriever.getResource(props.getProperty("sp.metadata"));
				if (ri != null) {
					InputStream is = ri.getInputStream();
					spConfig = new WsFedSPConfig(is);
				} else {
					LoggerUtils.getLogger(SAMLLogin.class).log(Level.SEVERE, "SAML: Unable to find SP configuration: \"" + props.getProperty("sp.metadata") + "\"");
				}
			}
			
		}
		
	}
	
}
