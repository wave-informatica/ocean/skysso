/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.saml.wsfed;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.io.File;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import javax.annotation.PostConstruct;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import org.w3c.dom.Document;

public class TokenManager {
	
	@Inject
	private Configuration config;
	
	private Key privateKey;
	
	int INDEX_VECTOR_LENGTH = 16;

	private static SecretKeySpec secretKey;
	private static byte[] ivByte;
	
	@PostConstruct
	public void init() {
		
		PersistedProperties props = config.getCustomProperties("skysso.saml");
		
		File pkFile = new File(props.getProperty("privateKey"));
		
		PrivateKeyReader p = new PrivateKeyReader();
		try {
			// privateKey = p.get("E:\\workspaceFiat\\adfs-token-response\\src\\test\\resources\\private_key_ADFS.der");  //private_key_ADFS.der
			//privateKey = p.get(new File("src/test/resources/private_key_ADFS.der").getAbsolutePath());
			//privateKey = p.get(TokenManager.class.getClassLoader().getResource("private_key_ADFS.der").getFile());
			
			privateKey = p.get(pkFile.getAbsolutePath());
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	public String getUserNameFromToken(String result) {
		
		Document doc = XmlUtil.getXMLFromString(result);
		
		String encKey = XmlUtil.readNode(doc, "/RequestSecurityTokenResponse/RequestedSecurityToken/EncryptedData/KeyInfo/EncryptedKey/CipherData/CipherValue");
		
		String simmKey = decryptRSA(encKey);
		
		String encToken = XmlUtil.readNode(doc, "/RequestSecurityTokenResponse/RequestedSecurityToken/EncryptedData/CipherData/CipherValue");
		
		String token = decryptAES(simmKey, encToken);
		
		Document tokenDoc = XmlUtil.getXMLFromString(token);
		
		return XmlUtil.readNode(tokenDoc, "/Assertion/AuthenticationStatement/Subject/NameIdentifier");
		
	}
	
	// Crypt con Pubblica RET codifica in BASE64
	public String encryptRSA(String payLoad) {
		// specify mode and padding instead of relying on defaults (use OAEP if available!)
		Cipher encrypt = null;
		try {
			encrypt = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding"); //RSA/ECB/PKCS1Padding
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		Key publicKey = null;

		PublicKeyReader p = new PublicKeyReader();
		try {
			//publicKey = p.get("E:\\temp\\ProveCertificati\\java\\public_key_ADFS.der"); //public_key_ADFS
			// publicKey = p.get(TokenManager.class.getClassLoader().getResource("public_key_ADFS.der").getFile());
			publicKey = p.get(new File("src/test/resources/public_key_ADFS.der").getAbsolutePath());
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		// init with the *public key*!ubli
		try {
			encrypt.init(Cipher.ENCRYPT_MODE, publicKey);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		// encrypt with known character encoding, you should probably use hybrid cryptography instead

		byte[] encryptedMessage = null;
		try {
			encryptedMessage = encrypt.doFinal(payLoad.getBytes());
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		//String ret = Base64.encodeBase64String(encryptedMessage);
		String ret = Base64.getEncoder().encodeToString(encryptedMessage);

		//System.out.println("Messaggio criptato con chiave pubblica:" + ret);
		return ret;
	}

	// DeCrypt con Privata IN: codifica BASE64  RET stringa decrittata
	public String decryptRSA(String encodeB64) {
		// specify mode and padding instead of relying on defaults (use OAEP if available!)
		Cipher decrypt = null;
		try {
			decrypt = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");  // "RSA/ECB/PKCS1Padding"
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}

		// init with the *public key*!ubli
		try {
			decrypt.init(Cipher.DECRYPT_MODE, privateKey);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		// encrypt with known character encoding, you should probably use hybrid cryptography instead

		byte[] decryptedMessage = null;
		try {
			//decryptedMessage = decrypt.doFinal(Base64.decodeBase64(encodeB64));
			decryptedMessage = decrypt.doFinal(Base64.getDecoder().decode(encodeB64));
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		String ret = null;
		//ret = Base64.encodeBase64String(decryptedMessage); //new String(decryptedMessage, "UTF-8");
		ret = Base64.getEncoder().encodeToString(decryptedMessage);
		//System.out.println("Chiave Simmetrica decriptata in B64:" + ret);
		return ret;
	}

	// Decript con Chiave Simmetrica
	public String decryptAES(String ChiaveSimmtEncB64, String strToDecrypt) {

		String ret = null;
		//byte[] ChiaveSimm = Base64.decodeBase64(ChiaveSimmtEncB64);
		byte[] ChiaveSimm = Base64.getDecoder().decode(ChiaveSimmtEncB64);
//    	try {
//			System.out.println("x:" + new String(ChiaveSimmCrypt, "UTF-8" ));
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}

// Calcolo IV (initialization vector)
//byte[] decStrToDecript = Base64.decodeBase64(strToDecrypt);
		byte[] decStrToDecript = Base64.getDecoder().decode(strToDecrypt);
		ivByte = Arrays.copyOf(decStrToDecript, INDEX_VECTOR_LENGTH); // estrae primi 16 bytes
		byte[] payLoad = Arrays.copyOfRange(decStrToDecript, INDEX_VECTOR_LENGTH, decStrToDecript.length); // estrae rimanenti bytes

// Set key
		secretKey = new SecretKeySpec(ChiaveSimm, "AES");

		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			// AES/CBC/NoPadding
			// AES/CBC/PKCS5Padding
			// NO AES/ECB/PKCS5PADDING

			//This class specifies an initialization vector (IV). Examples which use
			//IVs are ciphers in feedback mode, e.g., DES in CBC mode and RSA ciphers with OAEP encoding operation.
			IvParameterSpec ivParamsSpec = new IvParameterSpec(ivByte);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParamsSpec);
			ret = new String(cipher.doFinal(payLoad));
		} catch (Exception e) {
			System.out.println("Error while decrypting: " + e.toString());
		}

//System.out.println("String To Decrypt : " + strToDecrypt);
//System.out.println("Decrypted : " + TokenManager.getDecryptedString());
		return ret.trim();
	}

}
