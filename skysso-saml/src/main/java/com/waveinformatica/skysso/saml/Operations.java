/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.saml;

import com.lastpass.saml.AttributeSet;
import com.lastpass.saml.IdPConfig;
import com.lastpass.saml.SAMLClient;
import com.lastpass.saml.SAMLException;
import com.lastpass.saml.SAMLInit;
import com.lastpass.saml.SPConfig;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.ParametersBuilder;
import com.waveinformatica.skysso.saml.wsfed.TokenManager;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.exceptions.EncryptionException;
import com.waveinformatica.skysso.web.util.CryptUtils;
import java.io.File;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Properties;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

@OperationProvider(namespace = "auth")
public class Operations {
	
	@Inject
	private ObjectFactory factory;

	@Inject
	private UserSession userSession;

	@Inject
	private CoreController controller;

	@Inject
	private Configuration config;

	@Operation("saml/authenticate")
	@SkipAuthorization
	public BaseResult authenticate(HttpServletRequest httpReq) throws SAMLException {

		Properties props = config.getCustomProperties("skysso.saml");

		// at application startup, init library and create the client
		SAMLInit.initialize();
		IdPConfig idpConfig = new IdPConfig(new File(props.getProperty("idp.metadata")));
		SPConfig spConfig = new SPConfig(new File(props.getProperty("sp.metadata")));
		SAMLClient client = new SAMLClient(spConfig, idpConfig);

		// when a saml token is posted, extract the subject
		String authresponse = httpReq.getParameter("SAMLResponse");
		AttributeSet aset = client.validateResponse(authresponse);
		String user = aset.getNameId();

		SkySSOService service = controller.getService(SkySSOService.class);
		AuthenticatedUser authUser = new AuthenticatedUser(user, user);
		OceanUser oceanUser = service.validateUser(authUser);

		if (oceanUser != null) {
			userSession.login(oceanUser);
		}

		return new RedirectResult("~");

	}
	
	@Operation("wsfed/authenticate")
	@SkipAuthorization
	public BaseResult wsFedAuthenticate(
		  @Param("wresult") String result,
		  @Param("wctx") String wctx
	) throws EncryptionException {
		
		TokenManager tokenManager = factory.newInstance(TokenManager.class);
		
		String userName = tokenManager.getUserNameFromToken(result);
		
		if (StringUtils.isNotBlank(userName)) {
			
			AuthenticatedUser authUser = new AuthenticatedUser(userName, userName);
			
			SkySSOService service = controller.getService(SkySSOService.class);
			
			OceanUser user = service.validateUser(authUser);
			
			if (user != null) {
				
				userSession.login(user);
				
			}
			
		}
		
		String redirect = "~";
		
		if (StringUtils.isNotBlank(wctx)) {
			wctx = new String(CryptUtils.decryptBase64(wctx), Charset.forName("UTF-8"));
			
			ParametersBuilder builder = new ParametersBuilder(wctx);
			
			Map<String, String[]> params = builder.build();
			
			String[] redirectParams = params.get("redirect");
			
			if (redirectParams != null && redirectParams.length == 1 && StringUtils.isNotBlank(redirectParams[0])) {
				redirect = redirectParams[0];
			}
			
		}
		
		return new RedirectResult(redirect);
		
	}

}
