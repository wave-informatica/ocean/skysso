/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.skysso.saml;

import com.lastpass.saml.SAMLException;
import com.lastpass.saml.SPConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import javax.xml.namespace.QName;
import org.opensaml.Configuration;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.RoleDescriptor;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.parse.BasicParserPool;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author ivano
 */
public class WsFedSPConfig extends SPConfig {
	
	private String entityId;
	
	/**
	 * Construct a new, empty SPConfig.
	 */
	public WsFedSPConfig()
	{
		
	}
	/**
	 * Construct a new SPConfig from a metadata XML file.
	 *
	 * @param metadataFile File where the metadata lives
	 *
	 * @throws SAMLException if an error condition occurs while trying to parse and process
	 *              the metadata
	 */
	public WsFedSPConfig(File metadataFile)
		  throws SAMLException
	{
		FileInputStream inputStream;
		try {
			inputStream = new FileInputStream(metadataFile);
		}
		catch (java.io.IOException e) {
			throw new SAMLException(e);
		}
		
		try {
			init(inputStream);
		} finally {
			try {
				inputStream.close();
			}
			catch (java.io.IOException e) {
				//Ignore
			}
		}
	}
	
	/**
	 * Construct a new SPConfig from a metadata XML input stream.
	 *
	 * @param inputStream  An input stream containing a metadata XML document
	 *
	 * @throws SAMLException if an error condition occurs while trying to parse and process
	 *              the metadata
	 */
	public WsFedSPConfig(InputStream inputStream)
		  throws SAMLException
	{
		init(inputStream);
	}
	
	private void init(InputStream inputStream)
		  throws SAMLException
	{
		BasicParserPool parsers = new BasicParserPool();
		parsers.setNamespaceAware(true);
		
		EntityDescriptor edesc;
		
		try {
			Document doc = parsers.parse(inputStream);
			Element root = doc.getDocumentElement();
			
			UnmarshallerFactory unmarshallerFactory =
				  Configuration.getUnmarshallerFactory();
			
			edesc = (EntityDescriptor) unmarshallerFactory
				  .getUnmarshaller(root)
				  .unmarshall(root);
		}
		catch (org.opensaml.xml.parse.XMLParserException e) {
			throw new SAMLException(e);
		}
		catch (org.opensaml.xml.io.UnmarshallingException e) {
			throw new SAMLException(e);
		}
		
		// fetch sp information
		List<RoleDescriptor> spDescs = edesc.getRoleDescriptors();
		
		RoleDescriptor spDesc = null;
		for (RoleDescriptor rd : spDescs) {
			if (rd.isSupportedProtocol("http://docs.oasis-open.org/wsfed/federation/200706")) {
				spDesc = rd;
				break;
			}
		}
		
		/*if (spDesc == null)
			throw new SAMLException("No SP SSO descriptor found");
*/
		
		this.setEntityId(edesc.getEntityID());
	}
	
	public String getEntityId() {
		return entityId;
	}
	
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
}
