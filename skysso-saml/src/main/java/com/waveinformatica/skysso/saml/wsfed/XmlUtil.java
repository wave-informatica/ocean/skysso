/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.skysso.saml.wsfed;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlUtil {
	
	private String xml;
	
	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public static Document getXMLFromString(String xml) {
        org.w3c.dom.Document doc = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            factory.setNamespaceAware(false);
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            doc = (org.w3c.dom.Document) builder.parse(new InputSource(
                    new StringReader(xml)));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return doc;
    }

	public static String readNode(Document doc, String predicate) {
		String value = null;
	//	Document doc = getXMLFromString("<saml:Assertion MajorVersion=\"1\" MinorVersion=\"1\" AssertionID=\"_aa1226c7-6c3f-4df6-9383-e60c5e1bf6f5\" Issuer=\"http://sts-dev.fiat.com/adfs/services/trust\" IssueInstant=\"2017-08-22T12:16:05.697Z\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\"><saml:Conditions NotBefore=\"2017-08-22T12:16:05.681Z\" NotOnOrAfter=\"2017-08-22T13:16:05.681Z\"><saml:AudienceRestrictionCondition><saml:Audience>https://bestandard-cert.fcagroup.com/</saml:Audience></saml:AudienceRestrictionCondition></saml:Conditions><saml:AuthenticationStatement AuthenticationMethod=\"urn:oasis:names:tc:SAML:1.0:am:password\" AuthenticationInstant=\"2017-08-22T12:16:05.603Z\"><saml:Subject><saml:NameIdentifier>F43244A</saml:NameIdentifier><saml:SubjectConfirmation><saml:ConfirmationMethod>urn:oasis:names:tc:SAML:1.0:cm:bearer</saml:ConfirmationMethod></saml:SubjectConfirmation></saml:Subject></saml:AuthenticationStatement><ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><ds:Reference URI=\"#_aa1226c7-6c3f-4df6-9383-e60c5e1bf6f5\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><ds:Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>V21pd2SlRTZx95isax97XN0emzvXKxoCmUM13InUNbA=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>zpHimB7ZwslCQa+N1AKdL1nqhIT6BNkBYzXFF2eS2iGmVwzAdu0yT7muVXj/iqCsjyF74h8F/VELf0F206qAqIYYWABY+D77y5ANZlfZROUptsCn7mHHCHaPLwQXEYlrBpzpQHAjZ5sSE9+kf26A9ChCRkWpOHNTz3hQfu7s5s6S5eRabvTjgcqTST5DFXgyoLjDQ/KjgCnpawvEAr3Od+7PZ1YyIBdEAlrRYXE9gCeMPOLh42xrDd97aTch9u/FYOo69F4IFQQPgqG1TRSFkiMLsSoXNO18Nue/Mb2ixAgvAf+Rr5DGvq5Q2rgduMBJ7EUUmV86YYdVWP68jltz/w==</ds:SignatureValue><KeyInfo xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><X509Data><X509Certificate>MIIHBzCCBe+gAwIBAgIQdDwsFtPXQSI2IsRJMQjYtDANBgkqhkiG9w0BAQsFADB+MQswCQYDVQQGEwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xHzAdBgNVBAsTFlN5bWFudGVjIFRydXN0IE5ldHdvcmsxLzAtBgNVBAMTJlN5bWFudGVjIENsYXNzIDMgU2VjdXJlIFNlcnZlciBDQSAtIEc0MB4XDTE2MDUzMDAwMDAwMFoXDTE5MDUzMTIzNTk1OVowgZAxCzAJBgNVBAYTAklUMQ8wDQYDVQQIDAZUT1JJTk8xDzANBgNVBAcMBlRPUklOTzEcMBoGA1UECgwTRkNBIFNFUlZJQ0VTIFMuUC5BLjEaMBgGA1UECwwRSUNUIEFyY2hpdGVjdHVyZXMxJTAjBgNVBAMMHEFERlNTaWduaW5nLXN0cy1kZXYuZmlhdC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDPySlEk5d+X/3T0r7LpH6FrpZ2WeN0H8xhhMlGTG4ozW8jZKrbjSM26F7UleNG+g26ZaCpsOQLFNFJwz3Osk8JmI38djewHIF/m7xssfjrOSNEwpkKvZXg9gMBUFPw7c8DJ/H40Quc1ofsf8ye9L5xfA8vzJ/PAlolEbPWe8fHRNvRDw82ggD+Wes1xxA91wrUFeB44K9NlzG8E2vc35hcVcqV9KAJNRHOXefv+ovtKC/P1FCASUb4T23KHWuhL5V5FXTriBp7lrUbZ5on8HZEQvCP8ZChRrrgmPZYUC75Lbr5VEd36guH1mghEbTt8cfIEYvfnjpu9RVlTxNpra+HAgMBAAGjggNsMIIDaDAnBgNVHREEIDAeghxBREZTU2lnbmluZy1zdHMtZGV2LmZpYXQuY29tMAkGA1UdEwQCMAAwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjBhBgNVHSAEWjBYMFYGBmeBDAECAjBMMCMGCCsGAQUFBwIBFhdodHRwczovL2Quc3ltY2IuY29tL2NwczAlBggrBgEFBQcCAjAZDBdodHRwczovL2Quc3ltY2IuY29tL3JwYTAfBgNVHSMEGDAWgBRfYM9hkFXfhEMUimAqsvV69EMY7zArBgNVHR8EJDAiMCCgHqAchhpodHRwOi8vc3Muc3ltY2IuY29tL3NzLmNybDBXBggrBgEFBQcBAQRLMEkwHwYIKwYBBQUHMAGGE2h0dHA6Ly9zcy5zeW1jZC5jb20wJgYIKwYBBQUHMAKGGmh0dHA6Ly9zcy5zeW1jYi5jb20vc3MuY3J0MIIB9wYKKwYBBAHWeQIEAgSCAecEggHjAeEAdgDd6x0reg1PpiCLga2BaHB+Lo6dAdVciI09EcTNtuy+zAAAAVUCOwPmAAAEAwBHMEUCIQDE1g6e5GpCCyhqL0MaaE/hxqRRaltRJXGCA4DVLe0L3wIgeRJsM8ogLgNmLV4Ib3HbS+5l+mxg30lCuTK+MqI+S+cAdgCkuQmQtBhYFIe7E6LMZ3AKPDWYBPkb37jjd80OyA3cEAAAAVUCOwQIAAAEAwBHMEUCIQCSvbdchzw6M8D8Rvfe4SWUh2a5NWBpG0lZeKq5HktnswIgAVdlrYip2ldJoQj7L49TS5S/ZUGPDfxP7H/NPSyOBvYAdwBo9pj4H2SCvjqM7rkoHUz8cVFdZ5PURNEKZ6y7T0/7xAAAAVUCOwScAAAEAwBIMEYCIQCc82aW0SmL/OtGXBs/4riarBL0hQr85q2mwTP4a6HYEgIhAMrGldPJfpYxLMN/ch8L0NAu1znl/VlM9iumXLqTofYBAHYA7ku9t3XOYLrhQmkfq+GeZqMPfl+wctiDAMR7iXqo/csAAAFVAjsF5AAABAMARzBFAiEAj16LD3V6rZFBgSRsd3+lPixFwpuS3bUpNMKrsS/WZHQCIDDPTXA7/OPSmoBbcR+P28crFZJ9VVnf5I+SYZJ8itXqMA0GCSqGSIb3DQEBCwUAA4IBAQBMjXMO9PubGnJt6PCA9LKMm7sP87HTfzma4AuOfP2veoNYzwpqbAJnXyw1KmGDZ0vly7qP41fMI1LJ2Z4muLtN60vjYalNgmjJyTk3qLKHi3VLBDmH/9KnbxbuGRUk9qYTpY6hK0lELqZ8bbKkwwqTnka9Z24Hjb8TsCE0w49hUiENE092MCR6Objr6JzjQyQOkh45BZKabBD3tZBee6+dck0dgYunvyDKt+6VBZv8moC3QfPHQtZ5LPJp83d6FjbjDF6N676ybzVqgMSbF0MMaq9OCGaEqtNw1udPkvyyG9EiJ3yX21WMZu/AJsXFf09hXg6FV83HYuJOWtgSsM00</X509Certificate></X509Data></KeyInfo></ds:Signature></saml:Assertion>");
		
		//Document doc = getXMLFromString("<saml:Assertion><NameIdentifier>F43244A</NameIdentifier><xNameIdentifier>wwwwwwwww</xNameIdentifier></saml:Assertion>");
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		try {
			//XPathExpression expr = xpath.compile("/Assertion/AuthenticationStatement/Subject/NameIdentifier");
			XPathExpression expr = xpath.compile(predicate);
			value = (String) expr.evaluate(doc, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
	
 
	public static void main(String[] args) {
		XmlUtil xmlUtil = new XmlUtil();
		xmlUtil.setXml("<saml:Assertion MajorVersion=\"1\" MinorVersion=\"1\" AssertionID=\"_aa1226c7-6c3f-4df6-9383-e60c5e1bf6f5\" Issuer=\"http://sts-dev.fiat.com/adfs/services/trust\" IssueInstant=\"2017-08-22T12:16:05.697Z\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\"><saml:Conditions NotBefore=\"2017-08-22T12:16:05.681Z\" NotOnOrAfter=\"2017-08-22T13:16:05.681Z\"><saml:AudienceRestrictionCondition><saml:Audience>https://bestandard-cert.fcagroup.com/</saml:Audience></saml:AudienceRestrictionCondition></saml:Conditions><saml:AuthenticationStatement AuthenticationMethod=\"urn:oasis:names:tc:SAML:1.0:am:password\" AuthenticationInstant=\"2017-08-22T12:16:05.603Z\"><saml:Subject><saml:NameIdentifier>F43244A</saml:NameIdentifier><saml:SubjectConfirmation><saml:ConfirmationMethod>urn:oasis:names:tc:SAML:1.0:cm:bearer</saml:ConfirmationMethod></saml:SubjectConfirmation></saml:Subject></saml:AuthenticationStatement><ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><ds:Reference URI=\"#_aa1226c7-6c3f-4df6-9383-e60c5e1bf6f5\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><ds:Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>V21pd2SlRTZx95isax97XN0emzvXKxoCmUM13InUNbA=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>zpHimB7ZwslCQa+N1AKdL1nqhIT6BNkBYzXFF2eS2iGmVwzAdu0yT7muVXj/iqCsjyF74h8F/VELf0F206qAqIYYWABY+D77y5ANZlfZROUptsCn7mHHCHaPLwQXEYlrBpzpQHAjZ5sSE9+kf26A9ChCRkWpOHNTz3hQfu7s5s6S5eRabvTjgcqTST5DFXgyoLjDQ/KjgCnpawvEAr3Od+7PZ1YyIBdEAlrRYXE9gCeMPOLh42xrDd97aTch9u/FYOo69F4IFQQPgqG1TRSFkiMLsSoXNO18Nue/Mb2ixAgvAf+Rr5DGvq5Q2rgduMBJ7EUUmV86YYdVWP68jltz/w==</ds:SignatureValue><KeyInfo xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><X509Data><X509Certificate>MIIHBzCCBe+gAwIBAgIQdDwsFtPXQSI2IsRJMQjYtDANBgkqhkiG9w0BAQsFADB+MQswCQYDVQQGEwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xHzAdBgNVBAsTFlN5bWFudGVjIFRydXN0IE5ldHdvcmsxLzAtBgNVBAMTJlN5bWFudGVjIENsYXNzIDMgU2VjdXJlIFNlcnZlciBDQSAtIEc0MB4XDTE2MDUzMDAwMDAwMFoXDTE5MDUzMTIzNTk1OVowgZAxCzAJBgNVBAYTAklUMQ8wDQYDVQQIDAZUT1JJTk8xDzANBgNVBAcMBlRPUklOTzEcMBoGA1UECgwTRkNBIFNFUlZJQ0VTIFMuUC5BLjEaMBgGA1UECwwRSUNUIEFyY2hpdGVjdHVyZXMxJTAjBgNVBAMMHEFERlNTaWduaW5nLXN0cy1kZXYuZmlhdC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDPySlEk5d+X/3T0r7LpH6FrpZ2WeN0H8xhhMlGTG4ozW8jZKrbjSM26F7UleNG+g26ZaCpsOQLFNFJwz3Osk8JmI38djewHIF/m7xssfjrOSNEwpkKvZXg9gMBUFPw7c8DJ/H40Quc1ofsf8ye9L5xfA8vzJ/PAlolEbPWe8fHRNvRDw82ggD+Wes1xxA91wrUFeB44K9NlzG8E2vc35hcVcqV9KAJNRHOXefv+ovtKC/P1FCASUb4T23KHWuhL5V5FXTriBp7lrUbZ5on8HZEQvCP8ZChRrrgmPZYUC75Lbr5VEd36guH1mghEbTt8cfIEYvfnjpu9RVlTxNpra+HAgMBAAGjggNsMIIDaDAnBgNVHREEIDAeghxBREZTU2lnbmluZy1zdHMtZGV2LmZpYXQuY29tMAkGA1UdEwQCMAAwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjBhBgNVHSAEWjBYMFYGBmeBDAECAjBMMCMGCCsGAQUFBwIBFhdodHRwczovL2Quc3ltY2IuY29tL2NwczAlBggrBgEFBQcCAjAZDBdodHRwczovL2Quc3ltY2IuY29tL3JwYTAfBgNVHSMEGDAWgBRfYM9hkFXfhEMUimAqsvV69EMY7zArBgNVHR8EJDAiMCCgHqAchhpodHRwOi8vc3Muc3ltY2IuY29tL3NzLmNybDBXBggrBgEFBQcBAQRLMEkwHwYIKwYBBQUHMAGGE2h0dHA6Ly9zcy5zeW1jZC5jb20wJgYIKwYBBQUHMAKGGmh0dHA6Ly9zcy5zeW1jYi5jb20vc3MuY3J0MIIB9wYKKwYBBAHWeQIEAgSCAecEggHjAeEAdgDd6x0reg1PpiCLga2BaHB+Lo6dAdVciI09EcTNtuy+zAAAAVUCOwPmAAAEAwBHMEUCIQDE1g6e5GpCCyhqL0MaaE/hxqRRaltRJXGCA4DVLe0L3wIgeRJsM8ogLgNmLV4Ib3HbS+5l+mxg30lCuTK+MqI+S+cAdgCkuQmQtBhYFIe7E6LMZ3AKPDWYBPkb37jjd80OyA3cEAAAAVUCOwQIAAAEAwBHMEUCIQCSvbdchzw6M8D8Rvfe4SWUh2a5NWBpG0lZeKq5HktnswIgAVdlrYip2ldJoQj7L49TS5S/ZUGPDfxP7H/NPSyOBvYAdwBo9pj4H2SCvjqM7rkoHUz8cVFdZ5PURNEKZ6y7T0/7xAAAAVUCOwScAAAEAwBIMEYCIQCc82aW0SmL/OtGXBs/4riarBL0hQr85q2mwTP4a6HYEgIhAMrGldPJfpYxLMN/ch8L0NAu1znl/VlM9iumXLqTofYBAHYA7ku9t3XOYLrhQmkfq+GeZqMPfl+wctiDAMR7iXqo/csAAAFVAjsF5AAABAMARzBFAiEAj16LD3V6rZFBgSRsd3+lPixFwpuS3bUpNMKrsS/WZHQCIDDPTXA7/OPSmoBbcR+P28crFZJ9VVnf5I+SYZJ8itXqMA0GCSqGSIb3DQEBCwUAA4IBAQBMjXMO9PubGnJt6PCA9LKMm7sP87HTfzma4AuOfP2veoNYzwpqbAJnXyw1KmGDZ0vly7qP41fMI1LJ2Z4muLtN60vjYalNgmjJyTk3qLKHi3VLBDmH/9KnbxbuGRUk9qYTpY6hK0lELqZ8bbKkwwqTnka9Z24Hjb8TsCE0w49hUiENE092MCR6Objr6JzjQyQOkh45BZKabBD3tZBee6+dck0dgYunvyDKt+6VBZv8moC3QfPHQtZ5LPJp83d6FjbjDF6N676ybzVqgMSbF0MMaq9OCGaEqtNw1udPkvyyG9EiJ3yX21WMZu/AJsXFf09hXg6FV83HYuJOWtgSsM00</X509Certificate></X509Data></KeyInfo></ds:Signature></saml:Assertion>");
		//String val = xmlUtil.readNode("/Assertion/AuthenticationStatement/Subject/NameIdentifier");
		//System.out.println("val:" + val);
	
	}
}
