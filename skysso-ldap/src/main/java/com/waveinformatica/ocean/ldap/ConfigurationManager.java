/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.ldap;

import com.google.gson.Gson;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.Domain;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class ConfigurationManager {
	
	@Inject
	private CoreController controller;
	
	@Inject
	private Configuration config;
	
	public List<LdapInstance> getConfiguration(Domain domain) {
		
		List<LdapInstance> instances = new ArrayList<>();
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		Map<String, String> configuration = service.getDomainConfiguration(domain, "ldap");
		
		if (StringUtils.isNotBlank(configuration.get("ldap.count"))) {
			
			Gson gson = JsonUtils.getBuilder().create();
			
			int count = Integer.parseInt(configuration.get("ldap.count"));
			
			for (int i = 0; i < count; i++) {
				
				String cfg = configuration.get("ldap.config." + i);
				
				LdapInstance inst = gson.fromJson(cfg, LdapInstance.class);
				inst.setIndex(i);
				
				instances.add(inst);
				
			}
			
		}
		
		return instances;
		
	}
	
	public void save(Domain domain, LdapInstance instance) {
		
		List<LdapInstance> configs = getConfiguration(domain);
		
		if (instance.getIndex() == null) {
			configs.add(instance);
		}
		else {
			configs.set(instance.getIndex(), instance);
		}
		
		PersistedProperties props = config.getCustomProperties("skysso.ldap");
		
		props.clear();
		
		props.setProperty("ldap.count", String.valueOf(configs.size()));
		
		Gson gson = JsonUtils.getBuilder().create();
		
		for (int i = 0; i < configs.size(); i++) {
			props.setProperty("ldap.config." + i, gson.toJson(configs.get(i)));
		}
		
		try {
			props.store("");
		} catch (IOException ex) {
			LoggerUtils.getLogger(ConfigurationManager.class).log(Level.SEVERE, "Unable to store LDAP configuration properties", ex);
		}
		
	}
	
	public void delete(Domain domain, int index) {
		
		List<LdapInstance> configs = getConfiguration(domain);
		
		configs.remove(index);
		
		PersistedProperties props = config.getCustomProperties("skysso.ldap");
		
		props.clear();
		
		props.setProperty("ldap.count", String.valueOf(configs.size()));
		
		Gson gson = JsonUtils.getBuilder().create();
		
		for (int i = 0; i < configs.size(); i++) {
			props.setProperty("ldap.config." + i, gson.toJson(configs.get(i)));
		}
		
		try {
			props.store("");
		} catch (IOException ex) {
			LoggerUtils.getLogger(ConfigurationManager.class).log(Level.SEVERE, "Unable to store LDAP configuration properties", ex);
		}
		
	}
	
}
