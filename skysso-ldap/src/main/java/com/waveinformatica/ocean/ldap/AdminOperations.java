/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.ldap;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.Align;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.StackResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.input.HiddenFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.PasswordFieldHandler;
import com.waveinformatica.ocean.core.controllers.scopes.AdminScopeProvider.AdminRootScope;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.RequiresPermission;
import com.waveinformatica.skysso.web.dto.AuthResultCode;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;
import com.waveinformatica.skysso.web.dto.Permissions;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "admin/sso/ldap", inScope = AdminRootScope.class)
public class AdminOperations {
	
	@Inject
	private Configuration config;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Operation("")
	@RequiresPermission(Permissions.SYSADMIN)
	@RootOperation(section = "SkySSO", iconUrl = "key", title = "LDAP")
	public StackResult ldap(OperationScope opScope) {
		
		StackResult result = factory.newInstance(StackResult.class);
		result.setParentOperation("admin/");
		
		result.addOperation("admin/sso/ldap/settings");
		result.addOperation("admin/sso/ldap/test");
		

		Results.addCssSource(result, "ris/libs/bootstrap3-editable-1.5.1/css/bootstrap-editable.css");
		Results.addScriptSource(result, "ris/libs/bootstrap3-editable-1.5.1/js/bootstrap-editable.min.js");
		
		opScope.addBreadcrumbLink("admin/sso/ldap/", "LDAP");
		
		return result;
		
	}
	
	@Operation("settings")
	@RequiresPermission(Permissions.SYSADMIN)
	public TableResult settings(HttpServletRequest request) {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		ConfigurationManager ldapConfig = factory.newInstance(ConfigurationManager.class);
		
		SkySSOService service = controller.getService(SkySSOService.class);
		
		Domain domain = service.getDomain(request);
		
		result.setTableData(ldapConfig.getConfiguration(domain), LdapInstance.class);
		
		result.addTableOperation("admin/sso/ldap/edit", "new");
		result.addRowOperation("admin/sso/ldap/edit", "edit");
		result.addRowOperation("admin/sso/ldap/delete", "delete");
		
		result.setAlignRowOperations(Align.LEFT);
		
		return result;
		
	}
	
	@Operation("edit")
	public InputResult edit(@Param("index") Integer index, HttpServletRequest request) {
		
		InputResult result = factory.newInstance(InputResult.class);
		result.setParentOperation("admin/sso/ldap/");
		
		result.setAction("admin/sso/ldap/save");
		
		if (index != null) {

			SkySSOService service = factory.newInstance(SkySSOService.class);

			Domain domain = service.getDomain(request);

			ConfigurationManager configuration = factory.newInstance(ConfigurationManager.class);
			
			result.setData(configuration.getConfiguration(domain).get(index));
			
		}
		
		return result;
		
	}
	
	@Operation("save")
	public MessageResult save(
		  @Param("index") @InputResult.Field(handler = HiddenFieldHandler.class) Integer index,
		  @Param("server") @InputResult.Field(required = true) String server,
		  @Param("distinguishedName") @InputResult.Field(required = true) String distinguishedName,
		  @Param("user") @InputResult.Field(required = true) String user,
		  @Param("password") @InputResult.Field(required = true) String password,
		  HttpServletRequest request
	) {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.setParentOperation("admin/sso/ldap/");
		
		SkySSOService service = factory.newInstance(SkySSOService.class);
		
		Domain domain = service.getDomain(request);
		
		ConfigurationManager configuration = factory.newInstance(ConfigurationManager.class);
		
		LdapInstance inst = null;
		
		if (index != null) {
			inst = configuration.getConfiguration(domain).get(index);
		}
		else {
			inst = new LdapInstance();
		}
		
		inst.setServer(server);
		inst.setDistinguishedName(distinguishedName);
		inst.setUser(user);
		inst.setPassword(password);
		
		configuration.save(domain, inst);
		
		result.setMessage("LDAP instance successfully saved");
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setConfirmAction(result.getParentOperation());
		
		return result;
		
	}
	
	@Operation("delete")
	public MessageResult delete(@Param("index") Integer index, HttpServletRequest request) {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.setParentOperation("admin/sso/ldap/");
		
		SkySSOService service = factory.newInstance(SkySSOService.class);
		
		Domain domain = service.getDomain(request);
		
		ConfigurationManager configuration = factory.newInstance(ConfigurationManager.class);
		
		configuration.delete(domain, index);
		
		result.setMessage("LDAP instance successfully deleted");
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setConfirmAction(result.getParentOperation());
		
		return result;
		
	}
	
	@Operation("test")
	public InputResult testLdapAuth(
			@Param("userName") String userName,
			@Param("password") @InputResult.Field(handler = PasswordFieldHandler.class) String password,
			HttpServletRequest request
	) throws Exception {
		
		InputResult result = factory.newInstance(InputResult.class);
		
		if (StringUtils.isNotBlank(userName)) {
			
			if (StringUtils.isBlank(password)) {
				password = "";
			}
			
			LdapLogin authService = factory.newInstance(LdapLogin.class);
			
			SkySSOService service = controller.getService(SkySSOService.class);
			
			Domain domain = service.getDomain(request);
			
			LoginRequest loginRequest = new LoginRequest();
			loginRequest.setDomain(domain);
			loginRequest.setUserName(userName);
			loginRequest.setPassword(password);
			
			LoginResponse localResponse = authService.tryUserLogin(service, loginRequest);
			
			MessageResult message = factory.newInstance(MessageResult.class);
			
			if (localResponse == null) {
				message.setMessage("BUG: Authentication Backend Service \"" + authService.getClass().getName() + "\" returned NULL response");
				message.setType(MessageResult.MessageType.CRITICAL);
			}
			else if (localResponse.getResultCode() == null) {
				message.setMessage("BUG: Authentication Backend Service \"" + authService.getClass().getName() + "\" returned NULL result code");
				message.setType(MessageResult.MessageType.CRITICAL);
			}
			else if (localResponse.getResultCode() != AuthResultCode.OK) {
				message.setMessage("Authentication Backend Service \"" + authService.getClass().getName() + "\" returned " + localResponse.getResultCode().name() + " result code");
				message.setType(MessageResult.MessageType.CRITICAL);
			}
			else if (localResponse.getResultCode() == AuthResultCode.OK) {
				message.setMessage("Authentication successful");
				message.setType(MessageResult.MessageType.CONFIRM);
			}
			
			result.setMessage(message);
			
		}
		
		result.setAction("admin/sso/ldap/");
		result.setRefAction("admin/sso/ldap/test");
		
		return result;
		
	}
	
}
