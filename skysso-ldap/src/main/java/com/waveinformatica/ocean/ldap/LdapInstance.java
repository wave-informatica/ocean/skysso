/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.ldap;

import java.util.Collections;
import java.util.List;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.controllers.results.TableResult;

/**
 *
 * @author Ivano
 */
public class LdapInstance {
	
	@TableResult.TableResultColumn(key = true, hidden = true)
	@ExcludeSerialization
	private Integer index;
	private String server;
	private String distinguishedName;
	private String user;
	private String password;
	private List<String> usersBase;
	private List<String> usersFilters;
	private List<String> groupsBase;
	private List<String> groupsFilters;

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getDistinguishedName() {
		return distinguishedName;
	}

	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getUsersBase() {
		return usersBase;
	}

	public void setUsersBase(List<String> usersBase) {
		this.usersBase = usersBase;
	}

	public List<String> getUsersFilters() {
		if (usersFilters == null) {
			return Collections.emptyList();
		}
		return usersFilters;
	}

	public void setUsersFilters(List<String> usersFilters) {
		this.usersFilters = usersFilters;
	}

	public List<String> getGroupsBase() {
		return groupsBase;
	}

	public void setGroupsBase(List<String> groupsBase) {
		this.groupsBase = groupsBase;
	}

	public List<String> getGroupsFilters() {
		if (groupsFilters == null) {
			return Collections.emptyList();
		}
		return groupsFilters;
	}

	public void setGroupsFilters(List<String> groupsFilters) {
		this.groupsFilters = groupsFilters;
	}
	
}
