/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.waveinformatica.ocean.ldap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.dto.Domain;

public class LdapDao {
	
	private static final String LDAP_CTX_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	private ConfigurationManager manager;
	private SkySSOService service;
	
	@PostConstruct
	public void init() {
		manager = factory.newInstance(ConfigurationManager.class);
		service = controller.getService(SkySSOService.class);
	}
	
	public Map<String, List<Object>> getByDn(Domain domain, String dn, String ... attributes) {
		
		List<LdapInstance> instances = manager.getConfiguration(domain);
		
		for (LdapInstance instance : instances) {
			Map<String, List<Object>> res = getByDn(instance, dn, attributes);
			if (res != null) {
				return res;
			}
		}
		
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, List<Object>> getByDn(LdapInstance instance, String dn, String ... attributes) {
		
		try {
			
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_CTX_FACTORY);
			env.put(Context.PROVIDER_URL, instance.getServer());
			
			if (StringUtils.isNotBlank(instance.getUser())) {
				env.put(Context.SECURITY_PRINCIPAL, instance.getUser());
				env.put(Context.SECURITY_CREDENTIALS, instance.getPassword());
				env.put(Context.SECURITY_PRINCIPAL, instance.getDistinguishedName());
			}
			
	        LdapContext ctx = new InitialLdapContext(env, null);
	        ctx.setRequestControls(null);
	        
	        SearchControls searchControls = new SearchControls();
		    searchControls.setSearchScope(SearchControls.OBJECT_SCOPE);
		    searchControls.setTimeLimit(1);
	        
	        NamingEnumeration<?> namingEnum = null;
	        try {
	        	namingEnum = ctx.search(dn, "(objectClass=*)", searchControls);
	        } catch (NamingException e) {
	        	LoggerUtils.getLogger(LdapDao.class).log(Level.WARNING, e.getMessage());
	        	return null;
	        }
	        if (namingEnum == null) {
	        	return null;
	        }
	        if (namingEnum.hasMore()) {
	        	SearchResult result = (SearchResult) namingEnum.next();    
        		Attributes attrs = result.getAttributes ();
        		Map<String, List<Object>> item = new HashMap<String, List<Object>>();
        		
        		if (attributes.length > 0) {
        			for (String n : attributes) {
	        			Attribute attr = attrs.get(n);
        				List<Object> values = new ArrayList<>();
        				NamingEnumeration<?> vs = attr.getAll();
        				while (vs.hasMore()) {
        					values.add(vs.next());
        				}
        				item.put(attr.getID(), values);
        			}
        		}
        		else {
        			NamingEnumeration<? extends Attribute> allAttrs = attrs.getAll();
        			while (allAttrs.hasMore()) {
        				Attribute attr = allAttrs.next();
        				List<Object> values = new ArrayList<>();
        				NamingEnumeration<?> vs = attr.getAll();
        				while (vs.hasMore()) {
        					values.add(vs.next());
        				}
        				item.put(attr.getID(), values);
        			}
        		}
        		
        		return item;
	        }
			
		} catch (NamingException e) {
			LoggerUtils.getLogger(LdapDao.class).log(Level.SEVERE, null, e);
		}
		
		return null;
		
	}

	public List<Map<String, List<Object>>> searchUsers(Domain domain, String query, String ... attributes) {
		
		List<LdapInstance> instances = manager.getConfiguration(domain);
		
		List<Map<String, List<Object>>> finalResult = new ArrayList<>();
		
		for (LdapInstance li : instances) {
			finalResult.addAll(searchUsers(li, query, attributes));
		}
		
		return finalResult;
		
	}
	
	public List<Map<String, List<Object>>> searchUsers(HttpServletRequest request, String query, String ... attributes) {
		
		Domain domain = service.getDomain(request);
		
		List<LdapInstance> instances = manager.getConfiguration(domain);
		
		List<Map<String, List<Object>>> finalResult = new ArrayList<>();
		
		for (LdapInstance li : instances) {
			finalResult.addAll(searchUsers(li, query, attributes));
		}
		
		return finalResult;
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Map<String, List<Object>>> searchUsers(LdapInstance instance, String query, String ... attributes) {
		
		List<Map<String, List<Object>>> finalResult = new ArrayList<>();
		
		try {
			
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_CTX_FACTORY);
			env.put(Context.PROVIDER_URL, instance.getServer());
			
			if (StringUtils.isNotBlank(instance.getUser())) {
				env.put(Context.SECURITY_PRINCIPAL, instance.getUser());
				env.put(Context.SECURITY_CREDENTIALS, instance.getPassword());
				env.put(Context.SECURITY_PRINCIPAL, instance.getDistinguishedName());
			}
			
	        LdapContext ctx = new InitialLdapContext(env, null);
	        ctx.setRequestControls(null);
	        
	        SearchControls searchControls = getSimpleSearchControls(attributes);
	        
	        List<String> usersBase = instance.getUsersBase();
	        if (usersBase == null || usersBase.isEmpty()) {
	        	usersBase = Arrays.asList("");
	        }
	        for (String base : usersBase) {
	        	String localQuery = "";
	        	if (instance.getUsersFilters() != null && !instance.getUsersFilters().isEmpty()) {
	        		localQuery = "(|(" + StringUtils.join(instance.getUsersFilters(), ")(") + "))";
	        	}
	        	if (StringUtils.isNotBlank(query)) {
	        		localQuery = "(&" + localQuery + query + ")";
	        	}
	        	
	        	NamingEnumeration<?> namingEnum = ctx.search(base, localQuery, searchControls);
	        	while (namingEnum.hasMore ()) {
	        		SearchResult result = (SearchResult) namingEnum.next();    
	        		Attributes attrs = result.getAttributes ();
	        		Map<String, List<Object>> item = new HashMap<String, List<Object>>();
	        		
	        		if (attributes.length > 0) {
	        			for (String n : attributes) {
	        				if ("dn".equals(n)) {
	        					item.put("dn", (List) Arrays.asList(result.getName()));
	        				}
	        				else {
			        			Attribute attr = attrs.get(n);
		        				List<Object> values = new ArrayList<>();
		        				NamingEnumeration<?> vs = attr.getAll();
		        				while (vs.hasMore()) {
		        					values.add(vs.next());
		        				}
		        				item.put(attr.getID(), values);
	        				}
	        			}
	        		}
	        		else {
	        			if (Arrays.asList(attributes).contains("dn")) {
	        				item.put("dn", (List) Arrays.asList(result.getName()));
	        			}
	        			NamingEnumeration<? extends Attribute> allAttrs = attrs.getAll();
	        			while (allAttrs.hasMore()) {
	        				Attribute attr = allAttrs.next();
	        				List<Object> values = new ArrayList<>();
	        				NamingEnumeration<?> vs = attr.getAll();
	        				while (vs.hasMore()) {
	        					values.add(vs.next());
	        				}
	        				item.put(attr.getID(), values);
	        			}
	        		}
	        		
	        		finalResult.add(item);
	        		
	        	}
	        	namingEnum.close();
	        }
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		return finalResult;
		
	}
	
	private SearchControls getSimpleSearchControls(String...attributes) {
	    SearchControls searchControls = new SearchControls();
	    searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    searchControls.setTimeLimit(30000);
	    if (attributes.length > 0) {
	    	searchControls.setReturningAttributes(attributes);
	    }
	    //String[] attrIDs = {"objectGUID"};
	    //searchControls.setReturningAttributes(attrIDs);
	    return searchControls;
	}
	
}
