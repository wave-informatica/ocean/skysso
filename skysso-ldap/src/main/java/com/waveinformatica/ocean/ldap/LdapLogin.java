/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.ldap;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthResultCode;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;

/**
 *
 * @author Ivano
 */
@BackendAuthService("LDAP")
public class LdapLogin implements IBackendAuthService {
	
	private static final Logger logger = LoggerUtils.getLogger(LdapLogin.class);
	private static final String LDAP_CTX_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	
	@Inject
	private ObjectFactory factory;

	@Override
	public boolean isConfigured(SkySSOService service, Domain domain) {
		
		ConfigurationManager config = factory.newInstance(ConfigurationManager.class);
		
		return !config.getConfiguration(domain).isEmpty();
		
	}
	
	@Override
	public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {

		if (StringUtils.isBlank(request.getUserName())) {
			return new LoginResponse(AuthResultCode.USER_NOT_FOUND);
		}
		else if (StringUtils.isBlank(request.getPassword())) {
			return new LoginResponse(AuthResultCode.WRONG_PASSWORD);
		}
		
		ConfigurationManager config = factory.newInstance(ConfigurationManager.class);
		
		List<LdapInstance> ldaps = config.getConfiguration(request.getDomain());
		
		LoginResponse lastResponse = null;
		
		for (LdapInstance ldap : ldaps) {
			
			StringBuilder params = new StringBuilder();
			params.append(ldap.getServer());
			params.append(';');
			params.append(ldap.getUser());
			params.append(';');
			params.append(ldap.getPassword());
			params.append(';');
			params.append(ldap.getDistinguishedName());
			
			try {

				LoginResponse response = loginLdap2(params.toString(), request.getUserName(), request.getPassword(), AuthenticationMode.NORMAL);
				
				if (response.getResultCode() == AuthResultCode.OK) {
					return response;
				}
				else {
					if (lastResponse == null) {
						lastResponse = response;
					}
					else if (lastResponse.getResultCode().ordinal() > response.getResultCode().ordinal()) {
						lastResponse = response;
					}
				}

			} catch (Exception ex) {
				Logger.getLogger(LdapLogin.class.getName()).log(Level.SEVERE, null, ex);
				if (lastResponse == null) {
					lastResponse = new LoginResponse(AuthResultCode.ERROR);
				}
			}
			
		}
		
		return lastResponse;
		
	}
	
	/**
	 * Accesso e validazione informazioni utente.
	 * ------------
	 * Il programma effettua una catch dell'errore alla login
	 * utente. In base allo stato dell'utente (password sbagliata, password scaduta
	 * utente con password "resetta" il sistema AD ritorna una stringa di errore specifica
	 * benche'  l'eccezzione sia sempre la stessa (AuthenticationException)
	 * ------------
	 * @param serverName    Server AD che esporta le informazioni di login
	 * @param userSearch    Utente da validare (per esempio u102020)
	 * @param passwordUser    Password utente da validare
	 * @param userAccess    utente con cui e' necessario autenticarsi al servizio LDAP (vuoto = accesso pubblico)
	 * @param passwordAcces password utente con cui e' necessario autenticarsi al servizio LDAP (vuoto = accesso pubblico)
	 * @param distinguishedName contesto utente (distinguishedName) con cui e' necessario autenticarsi al servizio LDAP (vuoto = accesso pubblico)
	 * @throws NamingException    Errore in accesso a LDAP
	 */
	private static LoginResponse loginLdap2(String param, String userSearch, String passwordUser, AuthenticationMode mode) throws NamingException {
		
		String dnUser = "";
		String serverName = "";
		String userAccess = "";
		String passwordAcces = "";
		String distingushedName = "";
		
		int p1 = 0;
		int p2 = param.indexOf(";");
		
		if (p2 > 0) {
			serverName = param.substring(p1, p2);
			p1 = p2 + 1;
			p2 = param.indexOf(";", p1);
			if (p2 > 0) {
				userAccess = param.substring(p1, p2);
				p1 = p2 + 1;
				p2 = param.indexOf(";", p1);
				if (p2 > 0) {
					passwordAcces = param.substring(p1, p2);
					p1 = p2 + 1;
					if (p1 < param.length()) {
						distingushedName = param.substring(p1);
					}
				}
			}
		}
		
		// accede a DALP con l'utente e password specificati dell'applicazione
		DirContext ctx = execConnect(serverName, userAccess, passwordAcces, distingushedName);
		SearchControls ctls = openSearch(ctx);
		String filterSearch;
		
		if (mode == AuthenticationMode.LEGACY) {
			filterSearch = "sAMAccountName=" + userSearch;
		} else {
			//per ADAM
			filterSearch = "(|(cn=" + userSearch + ")(mail=" + userSearch + ")(uid=" + userSearch + "))";
		}
		NamingEnumeration answer = ctx.search("", filterSearch, ctls);
		if (answer.hasMoreElements()) {
			// Ottiene le informazioni base dell'utente
			SearchResult searchResult = (SearchResult) answer.next();
			Attributes attrs = searchResult.getAttributes();
			
			// Ottiene il distinguished name per fare l'accesso con utente password indicato
			dnUser = ga(attrs, "distinguishedName");
			if (dnUser == null) {
				dnUser = searchResult.getNameInNamespace();
			}
			
			try {
				
				// cerca di collegarsi con i dati forniti
				execConnect(serverName, userSearch, passwordUser, dnUser);
				
				AuthenticatedUser authUser = new AuthenticatedUser(userSearch.toUpperCase(), ga(attrs, "displayName"));
				authUser.getFields().put("name", ga(attrs, "givenName"));
				authUser.getFields().put("surname", ga(attrs, "sn"));
				
				return new LoginResponse(authUser);
				
			} catch (AuthenticationException e) {
				/*
				* In caso di errore password ritorna il seguente messaggio
				* "[LDAP: error code 49 - 80090308: LdapErr: DSID-0C090334, comment: AcceptSecurityContext error, data 52e, vece"
				*
				* In caso di password scaduta
				* "[LDAP: error code 49 - 80090308: LdapErr: DSID-0C09030F, comment: AcceptSecurityContext error, data 532, vece"
				*
				* In caso di password resettata e da impostare al primo logon
				* "[LDAP: error code 49 - 80090308: LdapErr: DSID-0C09030F, comment: AcceptSecurityContext error, data 773, vece"
				*/
				if (e.getMessage().indexOf("AcceptSecurityContext error, data 532") > 0) {
					logger.log(Level.FINEST, "*********Password scaduta.*********");
					return new LoginResponse(AuthResultCode.EXPIRED_PASSWORD);
				} else if (e.getMessage().indexOf("AcceptSecurityContext error, data 773") > 0) {
					logger.log(Level.FINEST, "*********Password resettata.*********");
					return new LoginResponse(AuthResultCode.RESET_PASSWORD);
				} else if (e.getMessage().indexOf("AcceptSecurityContext error, data 775") > 0) {
					logger.log(Level.FINEST, "*********Account locked.*********");
					return new LoginResponse(AuthResultCode.USER_ACCOUNT_LOCKED);
				} else if (e.getMessage().indexOf("AcceptSecurityContext error, data 52") > 0) {
					logger.log(Level.FINEST, "*********Password errata*********");
					return new LoginResponse(AuthResultCode.WRONG_PASSWORD);
				} else {
					logger.log(Level.SEVERE, "Unexpected error during user authentication", e);
					return new LoginResponse(AuthResultCode.ERROR);
				}
			}
			
			
		} else {
			logger.log(Level.FINEST, "Utente " + userSearch + " non trovato o non presente.");
		}
		return new LoginResponse(AuthResultCode.USER_NOT_FOUND);
	}
	
	/**
	 * Apre una sessione di ricerca in base
	 * alla dir context passata
	 * @param ctx Directory service context aperto su LDAP server
	 * @return
	 */
	private static SearchControls openSearch(DirContext ctx) {
		SearchControls ctls = new SearchControls();
		ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		ctls.setTimeLimit(10000);  // massimo 10 secondi
		ctls.setReturningAttributes(null);
		
		return ctls;
	}
	
	/**
	 * Apre una sessione di directory searc
	 * @param serverName    nome del server che espone i servizi LDAP
	 * @param userAccess    utente con cui ? necessario autenticarsi al servizio LDAP (vuoto = accesso pubblico)
	 * @param passwordAcces password utente con cui ? necessario autenticarsi al servizio LDAP (vuoto = accesso pubblico)
	 * @param distinguishedName contesto utente (distinguishedName) con cui ? necessario autenticarsi al servizio LDAP (vuoto = accesso pubblico)
	 * @return Directory service interface LDAP aperta
	 * @throws NamingException
	 */
	private static DirContext execConnect(String serverName, String userAccess, String passwordAcces, String distinguishedName) throws NamingException {
		String jndiProvider = serverName;
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_CTX_FACTORY);
		env.put(Context.PROVIDER_URL, jndiProvider);
		
		if (!userAccess.equals("")) {
			env.put(Context.SECURITY_PRINCIPAL, userAccess);
			env.put(Context.SECURITY_CREDENTIALS, passwordAcces);
			env.put(Context.SECURITY_PRINCIPAL, distinguishedName);
		}
		
		DirContext ctx = new InitialDirContext(env);
		
		return ctx;
	}
	
	private static String ga(Attributes sr, String nome) throws NamingException {
		Attribute v = sr.get(nome);
		if (v == null) {
			return null;
		}
		return v.get(v.size() - 1).toString();
	}

	@Override
	public UserNameType getUserNameType() {
		return UserNameType.TEXT;
	}

	@Override
	public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
		return null;
	}
	
}
