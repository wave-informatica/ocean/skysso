/*******************************************************************************
 * Copyright 2019 Wave Informatica S.r.l.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.skysso.mail;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.skysso.web.SkySSOService;
import com.waveinformatica.skysso.web.annotations.BackendAuthService;
import com.waveinformatica.skysso.web.base.IBackendAuthService;
import com.waveinformatica.skysso.web.base.UserNameType;
import com.waveinformatica.skysso.web.dto.AuthResultCode;
import com.waveinformatica.skysso.web.dto.AuthenticatedUser;
import com.waveinformatica.skysso.web.dto.AuthenticationLink;
import com.waveinformatica.skysso.web.dto.Domain;
import com.waveinformatica.skysso.web.dto.LoginRequest;
import com.waveinformatica.skysso.web.dto.LoginResponse;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@BackendAuthService("mail")
public class MailBackendAuthService implements IBackendAuthService {

    @Override
    public boolean isConfigured(SkySSOService service, Domain domain) {
        
        Map<String, String> params = service.getDomainConfiguration(domain, "mail");
        
        if (StringUtils.isBlank(params.get("port"))) {
            return false;
        }
        
        try {
            
            int port = Integer.parseInt(params.get("port"));
            
            if (port <= 0 || port >= 65535) {
                return false;
            }
            
        } catch (NumberFormatException e) {
            return false;
        }
        
        return StringUtils.isNotBlank(params.get("protocol")) &&
                StringUtils.isNotBlank(params.get("host"));
        
    }

    @Override
    public LoginResponse tryUserLogin(SkySSOService service, LoginRequest request) {
        
        try {
            
            Map<String, String> params = service.getDomainConfiguration(request.getDomain(), "mail");
            
            Properties props = new Properties();
            
            boolean useSsl = params.containsKey("useSsl") ? Boolean.valueOf(params.get("useSsl")) : false;
            
            Session session = Session.getDefaultInstance(props, null);
            Store store = session.getStore(useSsl ? params.get("protocol") + "s" : params.get("protocol"));
            store.connect(params.get("host"), Integer.parseInt(params.get("port")), request.getUserName(), request.getPassword());
            
            store.close();
            
            AuthenticatedUser user = new AuthenticatedUser(request.getUserName().trim().toLowerCase(), request.getUserName().substring(0, request.getUserName().indexOf('@')));
            user.getFields().put("email", request.getUserName());
            
            return new LoginResponse(user);
            
        } catch (NoSuchProviderException ex) {
            LoggerUtils.getLogger(MailBackendAuthService.class).log(Level.SEVERE, "Unexpected error logging user through Mail", ex);
            return new LoginResponse(AuthResultCode.ERROR);
        } catch (MessagingException ex) {
            LoggerUtils.getLogger(MailBackendAuthService.class).log(Level.WARNING, "Unable to authenticate user through Mail", ex);
            return new LoginResponse(AuthResultCode.USER_NOT_FOUND);
        }
        
    }

    @Override
    public UserNameType getUserNameType() {
        return UserNameType.EMAIL;
    }

    @Override
    public List<AuthenticationLink> getExternalAuthenticationLinks(BaseResult result) {
        return null;
    }
    
}
